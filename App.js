/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import AppNavigator from './navigation/AppNavigator';
import {ApolloProvider} from 'react-apollo';
import {client} from './api';
import ErrorBoundary from './components/error/ErrorBoundary';

class App extends Component{
  render(){
    return(
      <ErrorBoundary>
        <ApolloProvider client={client}>
          <AppNavigator />
        </ApolloProvider>
      </ErrorBoundary>
    )
  }
}

export default App;
