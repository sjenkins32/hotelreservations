import React from 'react';
import {
  View,
  Text,
  Alert
} from 'react-native';
import {gql} from 'apollo-boost';
import { Mutation } from 'react-apollo';
import SubmitButton from '../components/functional/SubmitButton';

export const GET_RESERVATIONS = gql`
 {
    reservations{
      id
      name
      hotelName
      arrivalDate
      departureDate
    }
 }
`

const CREATE_RESERVATION = gql`
  mutation CreateReservation($name: String!, $hotelName: String!, $departureDate: String!, $arrivalDate: String!){
    createReservation( data:
      {
        name: $name, 
        hotelName: $hotelName, 
        departureDate: $departureDate,
        arrivalDate: $arrivalDate}
    ){
      name
      hotelName
      departureDate
      arrivalDate
    }
  }
`
/*
export const DELETE_RESERVATION = gql`
  mutation DeleteReservation($id: String!) {
    deleteReservation( where: {
      id: $id
    }){
      id
    }
  }
`
*/


const ReservationAPI = (props) => {
  let whitespaceCharSearch                  = /\s/g;
  
  return(
      <Mutation 
        mutation={CREATE_RESERVATION}>
        {(createReservation, { loading, error }) => (
          <View style={{flex: 1}}>
            {(!loading && !error) && <SubmitButton 
                color={props.textColor}
                backgroundColor={props.bgColor}
                onSubmit={() => {
                  if(props.name !== "" && props.departureDate !== "" && props.arrivalDate !== "" && props.hotelName !== ""){
                    if(props.name.length > 4){
                      if(props.name.match(whitespaceCharSearch) !== null){
                
                        return createReservation({
                          variables: {name: props.name, hotelName: props.hotel, departureDate: props.departureDate, arrivalDate: props.arrivalDate},
                          refetchQueries: [{ 
                            query:  GET_RESERVATIONS
                          }]
                        }).then((data) => {if(data){Alert.alert("Saved Successfully!!")}})
                      } else {
                        Alert.alert("Full name needs to be provided")
                      }
                    } else {
                      Alert.alert("Name field needs to be greater than 4 characters")
                    }
                  } else {
                    Alert.alert("Fields cannot be empty")
                  }
                }}
              />}
            {loading && <Text style={{fontSize: 30, alignSelf: 'center'}}>Loading...</Text>}
            {error && <Text style={{fontSize: 30, alignSelf: 'center'}}>An Error Occurred</Text>}
          </View>
        )}
      </Mutation>
  )
}

export default ReservationAPI;
