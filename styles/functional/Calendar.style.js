import { 
    StyleSheet,
    Dimensions
  } from 'react-native';
  
export default StyleSheet.create({
  view: {
    flex: 1, 
    height: 50
  },
  includesSelectedDates: {
    marginHorizontal: '5%', 
    backgroundColor:"white", 
    width: 18, 
    alignSelf: 'center', 
    textAlign: 'center'
  },
  notIncludesSelectedDates: {
    marginHorizontal: '5%', 
    color: 'white', 
    width: 18, 
    alignSelf: 'center', 
    textAlign: 'center'
  },
  calendarControlsView: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-evenly',
    marginTop: 50
  },
  calendarControls: {
    fontSize: 20
  },
  flatlist: {
    backgroundColor: '#0b1c3a'
  },
  flatlistContentContainer: {
    alignSelf: 'center'
  },
  flatlistColumWrapper: {
    justifyContent: 'space-evenly', 
    marginVertical: 10
  },
  flatlistListHeaderText: {
    color: 'white', 
    marginHorizontal: '5%'
  },
  flatlistListHeaderView: {
    flexDirection: 'row', 
    justifyContent: 'space-evenly', 
    marginTop: 20
  }
})  
