import { 
  StyleSheet,
  Dimensions
} from 'react-native';
  
export default StyleSheet.create({
  nameLabel: {
    alignSelf: 'center', 
    fontSize: 20, 
    marginTop: '7%'
  },
  textInput: {
    flex: 1,
    backgroundColor: '#0b1c3a',
    color: 'white', 
    width: Dimensions.get('window').width, 
    height: 50,
    alignSelf: 'center',
    borderWidth: 2, 
    borderColor: 'black',
    borderRadius: 8,
    textAlign: 'center',
    fontSize: 25
  },
})  
  