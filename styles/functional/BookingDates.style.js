import { 
    StyleSheet,
    Dimensions
  } from 'react-native';
  
export default StyleSheet.create({
  selectedDatesLabelView: {
    marginTop: 20,
    flex: 3, 
    flexDirection: 'row', 
    justifyContent: 'space-evenly', 
  },
  arriveLabel: {
    fontSize: 20, 
    color: 'black',
    flex: 1
  },
  leavingLabel: {
    fontSize: 20, 
    color: 'black',
    flex: 1
  },
  selectedDatesView: {
    flex: 3, 
    flexDirection: 'row', 
    justifyContent: 'space-evenly', 
    height: 50
  },
  departureDate: {
    fontSize: 20, 
    color: 'white',
    backgroundColor: '#0b1c3a',
    borderColor: 'gray',
    borderWidth: 2,
    height: 40,
    borderRadius: 4,
    alignSelf: 'center',
    flex: 1,
    textAlignVertical: 'center',
  },
  arrivalDate: {
    fontSize: 20, 
    color: 'white',
    backgroundColor: '#0b1c3a',
    borderColor: 'gray',
    borderWidth: 2,
    height: 40,
    borderRadius: 4,
    alignSelf: 'center',
    flex: 1,
    textAlignVertical: 'center',
  }
})  
