import { 
  StyleSheet
} from 'react-native';
  
export default StyleSheet.create({
  selectHotelLabel: {
    alignSelf: 'center', 
    fontSize: 20, 
    marginTop: '7%'
  },
  picker: {
    borderWidth: 4, 
    borderColor: '#0b1c3a'
  },
})  
