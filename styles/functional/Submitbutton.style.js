import { 
  StyleSheet
} from 'react-native';
  
export default StyleSheet.create({
  submitButton: {
    flex: 1, 
    textAlign: 'center', 
    fontSize: 30
  }
})  
