import { 
    StyleSheet,
    Dimensions
  } from 'react-native';
  
export default StyleSheet.create({
  imageBackground: {
    backgroundColor: 'white',
    borderColor: 'gray',
    borderWidth: 3,
    height: 150,
    borderRadius: 20,
    flex: 1, 
    flexDirection: 'column'
  },
  messageStyle: {
    alignSelf:'center', 
    marginTop: Dimensions.get('window').height * .5, 
    fontSize: 30
  },  
  existingReservationsLabel: {
    color: 'black'
  },
  view: {
    backgroundColor: 'white'
  }, 
  childViews: {
    flex: 1, 
    justifyContent: 'space-between', 
    flexDirection: 'column', 
    alignSelf: 'center'
  }
})  
