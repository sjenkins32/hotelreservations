import React from 'react';
import { render } from 'react-native-testing-library';
import BookingDates from '../components/functional/BookingDates';

describe('BookingDates', () => {
  it('Matches screenshot', () => {
    const { component } = render(<BookingDates />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
