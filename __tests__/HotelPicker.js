import React from 'react';
import { render } from 'react-native-testing-library';
import HotelPicker from '../components/functional/HotelPicker';

describe('HotelPicker', () => {
  it('Matches screenshot', () => {
    const { component } = render(<HotelPicker />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
