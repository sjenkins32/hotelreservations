import React from 'react';
import { render } from 'react-native-testing-library';
import Calendar from '../components/functional/Calendar';

describe('Calendar', () => {
  it('Matches screenshot', () => {
    const { component } = render(<Calendar />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
