import React from 'react';
import { render } from 'react-native-testing-library';
import AddReservation from '../components/functional/AddReservation';

describe('AddReservation', () => {
  it('Matches screenshot', () => {
    const { component } = render(<AddReservation />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
