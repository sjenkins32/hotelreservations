import React from 'react';
import { render } from 'react-native-testing-library';
import NameInput from '../components/functional/NameInput';

describe('NameInput', () => {
  it('Matches screenshot', () => {
    const { component } = render(<NameInput />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
