import React from 'react';
import { render } from 'react-native-testing-library';
import SubmitButton from '../components/functional/SubmitButton';

describe('SubmitButton', () => {
  it('Matches screenshot', () => {
    const { component } = render(<SubmitButton />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
