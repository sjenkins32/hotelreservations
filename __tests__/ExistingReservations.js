import React from 'react';
import { render } from 'react-native-testing-library';
import ExistingReservations from '../components/functional/ExistingReservations';

describe('ExistingReservations', () => {
  it('Matches screenshot', () => {
    const { component } = render(<ExistingReservations />).toJSON();
    expect(component).toMatchSnapshot();
  })
})
