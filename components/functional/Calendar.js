/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import {
  View,
  Text,
  FlatList
} from 'react-native';
import _ from 'lodash';
import styles from '../../styles/functional/Calendar.style';

const Calendar = (props) => {

  const _renderItem = ({item}) => {
    const date = _.find(props.selectedDates, {"dateAsString": `${item.month} ${item.day}, 2019`})
    
    return(
      <View style={styles.view}>
        {
          (_.includes(props.selectedDates, date))
          ?
          <Text 
            style={styles.includesSelectedDates} 
            onPress={() => {props._selectDate(item.day)}}>
            {item.day}
          </Text> 
          :
          <Text 
            style={styles.notIncludesSelectedDates}
            onPress={() => {props._selectDate(item.day)}}>
            {item.day}
          </Text>
        }
      </View>
    )
  }

  return(
    <View>
      <View style={styles.calendarControlsView}>
        <Text
          style={styles.calendarControls}
          onPress={props._prevMonth}>
          Prev
        </Text>
        <Text
          style={styles.calendarControls}>
          {props.getMonth}
        </Text>
        <Text
          style={styles.calendarControls}
          onPress={props._nextMonth}>
          Next
        </Text>
      </View>
      <FlatList
        data={props._formatDates}
        renderItem={_renderItem}
        numColumns={7}
        style={styles.flatlist}
        contentContainerStyle={styles.flatlistContentContainer}
        columnWrapperStyle={styles.flatlistColumWrapper}
        keyExtractor={(item, index) => index}
        ListHeaderComponent={
          <View style={styles.flatlistListHeaderView}>
            <Text style={styles.flatlistListHeaderText}>SUN</Text>
            <Text style={styles.flatlistListHeaderText}>MON</Text>
            <Text style={styles.flatlistListHeaderText}>TUE</Text>
            <Text style={styles.flatlistListHeaderText}>WED</Text>
            <Text style={styles.flatlistListHeaderText}>THU</Text>
            <Text style={styles.flatlistListHeaderText}>FRI</Text>
            <Text style={styles.flatlistListHeaderText}>SAT</Text>
          </View>
        }
      />
    </View>        
  )
}

export default Calendar;
