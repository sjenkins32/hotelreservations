import React from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import styles from '../../styles/functional/NameInput.style';

const NameInput = (props) => {
  return(
    <View style={{flex: 1}}>
      <Text style={styles.nameLabel}>
        Enter Name:
      </Text>    
      <TextInput
        style={styles.textInput}
        placeholder='Enter your name'
        onChangeText={props.nameInput}
        editable
        numberOfLines={1}
        value={props.name}
        maxLength={30}/>
    </View>  
  )
}

export default NameInput;
