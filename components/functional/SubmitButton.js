/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import {
  Text
} from 'react-native';
import styles from '../../styles/functional/Submitbutton.style'

const SubmitButton = (props) => {
  return(
    <Text
      style={[styles.submitButton, {backgroundColor: props.backgroundColor, color: props.color}]}
      onPress={props.onSubmit}>
      BOOK NOW!!
    </Text>
  )
}

export default SubmitButton;
