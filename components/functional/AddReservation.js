/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import {
  View,
  Text,
  FlatList
} from 'react-native';
import styles from '../../styles/functional/AddReservation.style';

const AddReservation = (props) => {
  return(
    <View styles={styles.view}>
      <Text style={styles.text}>
        Add a Reservation
      </Text>
      <FlatList
        data={props.data}
        renderItem={props.renderItem}
        extraData={props.extraData}
        keyExtractor={props.keyExtractor}
      />
    </View>
  )
}

export default AddReservation;
