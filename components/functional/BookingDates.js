import React from 'react';
import {
  View,
  Text
} from 'react-native';
import styles from '../../styles/functional/BookingDates.style';

const BookingDates = (props) => {
  return(
    <View>
      <View style={styles.selectedDatesLabelView}>
        <Text style={styles.arriveLabel}>
          Check-in Date
        </Text>
        <Text style={styles.leavingLabel}>
          Check-out Date
        </Text>
      </View>
      <View style={styles.selectedDatesView}>
        <Text style={styles.departureDate}>
          {props.departureDate}
        </Text>
        <Text style={styles.arrivalDate}>
          {props.arrivalDate}
        </Text>
      </View>
    </View>
  )
}

export default BookingDates;