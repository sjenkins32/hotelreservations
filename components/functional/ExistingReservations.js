/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import {
  View,
  FlatList
} from 'react-native';
import styles from '../../styles/functional/ExistingReservation.style';

const ExistingReservations = (props) => {
  return(
    <View styles={styles.view}>
      <FlatList
        data={props.data}
        renderItem={props.renderItem}
        extraData={props.extraData}
        keyExtractor={props.keyExtractor}
      />
    </View>
  )
}

export default ExistingReservations;
