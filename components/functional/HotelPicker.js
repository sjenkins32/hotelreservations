import React from 'react';
import {
  View,
  Text,
  Picker
} from 'react-native';
import styles from '../../styles/functional/Picker.style';

const HotelPicker = (props) => {
  return(
    <View style={{flex: 1}}>
      <Text style={styles.selectHotelLabel}>
          Select Hotel:
      </Text>    
      <Picker
        style={styles.picker}
        selectedValue={props.hotel}
        onValueChange={props.hotelSelection}>
        <Picker.Item label="Hilton" value="Hilton" />
        <Picker.Item label="Mariott" value="Mariott" />
        <Picker.Item label="Sheraton" value="Sheraton" />
        <Picker.Item label="Westin" value="Westin" />
        <Picker.Item label="Four Seasons" value="Four Seasons" />
        <Picker.Item label="Ritz-Carlton" value="Ritz-Carlton" />
        <Picker.Item label="Hyatt" value="Hyatt" />
        <Picker.Item label="Renaissance" value="Renaissance" />
        <Picker.Item label="Embassy Suites" value="Embassy Suites" />
      </Picker> 
    </View>  
  )
}

export default HotelPicker;
