/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 * 
 * By Sherrod Jenkins
 */

import React from 'react';
import {
  ScrollView, 
  View,
  KeyboardAvoidingView,
} from 'react-native';
import Calendar from '../functional/Calendar';
import BookingDates from '../functional/BookingDates';
import HotelPicker from '../functional/HotelPicker';
import NameInput from '../functional/NameInput';
import ReservationAPI from '../../api/reservations';

const Booking = (props) => {
  return(
    <KeyboardAvoidingView 
      style={{flex: 1}} behavior="padding" enabled
      contentContainerStyle={{justifyContent: 'flex-end'}}>
      <ScrollView
        style={{flex: 1}}>
        <BookingDates
          departureDate={props.departureDate}
          arrivalDate={props.arrivalDate}
        />
        <Calendar 
          _formatDates={props._formatDates}
          selectedDates={props.selectedDates}
          _selectDate={props._selectDate}
          _prevMonth={props._prevMonth}
          _nextMonth={props._nextMonth}
          getMonth={props.getMonth}
        />
        <HotelPicker
          hotel={props.hotel}
          hotelSelection={props._hotelSelection}
        />
        <NameInput
          nameInput={props._nameInput}
          name={props.name}
        />
      </ScrollView>
      <View style={{flex: .13}}>
        <ReservationAPI
          name={props.name}
          hotel={props.hotel}
          departureDate={props.departureDate}
          arrivalDate={props.arrivalDate}
          bgColor={props.bgColor}
          textColor={props.textColor}
        />
      </View>
    </KeyboardAvoidingView>
  )
}

export default Booking;
