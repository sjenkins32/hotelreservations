import React, {Component} from 'react';
import { 
  View,
  Text 
} from 'react-native';
import {graphql} from 'react-apollo';
import {GET_RESERVATIONS} from '../../api/reservations';
import ExistingReservations from '../functional/ExistingReservations';
import styles from "../../styles";

class Home extends Component {
  static navigationOptions = {
    title: 'Reservations',
    headerStyle: {
      backgroundColor: 'white'
    },
    headerTitleStyle: {
      color: 'gray',
      fontSize: 20
    },
  };
  
  render(){
    const { data: { loading, error, reservations} } = this.props;
    if (loading) {
      return <Text style={styles.messageStyle}>Loading...</Text>;
    }
    if (error) {
      return <Text style={styles.messageStyle}>Error!</Text>;
    }

    return(
      <View style={styles.view}>
        <ExistingReservations
          data={reservations}
          renderItem={({item}) => 
            <View style={styles.imageBackground}>
              <View style={styles.childViews}>
                <Text style={styles.existingReservationsLabel}>
                    Name: 
                  <Text style={styles.existingReservationsLabel}>
                    {item.name}
                  </Text>
                </Text>
              </View>
              <View style={styles.childViews}>
                <Text style={styles.existingReservationsLabel}>
                  Hotel Name:
                  <Text style={styles.existingReservationsLabel}>
                    {item.hotelName} 
                  </Text>
                </Text>
              </View>
              <View style={styles.childViews}>
                <Text style={styles.existingReservationsLabel}>
                  Arrival Date:
                  <Text style={styles.existingReservationsLabel}>
                    {item.arrivalDate} 
                  </Text>  
                </Text>
              </View>
              <View style={styles.childViews}>
                <Text style={styles.existingReservationsLabel}>
                  Departure Date:
                  <Text style={styles.existingReservationsLabel}>
                    {item.departureDate}
                  </Text>
                </Text>
              </View>
          </View>}
          keyExtractor={(item) => item.id}
          extraData={this.props}
        />
      </View> 
    )
  }
}

export default graphql(GET_RESERVATIONS)(Home);
