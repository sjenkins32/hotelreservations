/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 * 
 * By Sherrod Jenkins
 */

import React, { Component } from 'react';
import Booking from '../class/Bookings';
import _ from 'lodash';

interface State {
  hotel?: string, 
  name?: string,
  arrivalDate?: string,
  departureDate?: string,
  date?: number,
  fullDate?: string,
  day?: number,
  touched?: Boolean,
  dateChange?: Boolean,
  currentMonth?: string[]
}

interface currentMonth {
  month: string,
  days: string
}

class BookingsContainer extends Component<State> {
  /*
    Initial State
  */

  state = {
    dates: [],
    hotel: '', 
    name: '',
    selectedDates: [],
    arrivalDate: "",
    departureDate: "",
    date: new Date().getMonth(),
    fullDate: new Date().toString().substring(0,3),
    day: new Date().getDate(),
    currentMonth: {} as currentMonth,
    formatDatesArray: [],
    removeDate: false,
    dateChange: false,
    months: [
      {"month":"January", "days": "31"},
      {"month":"February", "days": "28"},
      {"month":"March", "days": "31"},
      {"month":"April", "days": "30"},
      {"month":"May", "days": "31"}, 
      {"month":"June", "days": "30"}, 
      {"month":"July", "days": "31"},
      {"month":"August", "days": "31"},
      {"month":"September", "days": "30"},
      {"month":"October", "days": "31"},
      {"month":"November", "days": "30"},
      {"month":"December", "days": "31"}
    ], 
    bgColor: '#0b1c3a',
    textColor: 'gray'
  }

  /*
    Navigation Config
  */  
  static navigationOptions = {
    title: 'Bookings',
    headerStyle: {
      backgroundColor: 'white'
    },
    headerTitleStyle: {
      color: 'gray',
      fontSize: 20
    },
  };

  /*
    Lifecycle Methods
  */
  componentDidMount() {
    this.setState(function(){
      return{
        currentMonth: this.state.months[this.state.date]
      }
    })
  }

  componentDidUpdate(prevProps, prevState){
    if(!_.isEqual(this.state.selectedDates, prevState.selectedDates)){
      this._sortSelectedDates()
      this.setState(function(){
        if(this.state.selectedDates.length > 1){
          return{ 
            departureDate: this._sortSelectedDates()[0].dateAsString,
            arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
          }
        } else {
          return{
            departureDate: this._sortSelectedDates()[0].dateAsString,
          }
        }
      })
    } else if(!_.isEqual(this.state.date, prevState.date)){
      this.setState(function(){
        return{
          currentMonth: this.state.months[this.state.date]
        }
      })
    } else if(!_.isEqual(this.state.name, prevState.name)){
        this._validateFields()
    } else if(!_.isEqual(this.state.hotel, prevState.hotel)){
      this._validateFields()
    } else if(!_.isEqual(this.state.departureDate, prevState.departureDate)){
      this._validateFields()
    } else if(!_.isEqual(this.state.arrivalDate, prevState.arrivalDate)){
      this._validateFields()
    }     
  }

  /*
    Functions
  */
  _sortSelectedDates = () => {
    let sortedDayArray = _.sortBy(this.state.selectedDates, [function(a){ return a.dateAsNumber}])
    let sortedMonthArray = _.sortBy(sortedDayArray, [function(a){ return a.monthAsNumber}])

    return sortedMonthArray
  }

  _selectDate = (date: number) => {
    let currentMonthLength = this.state.currentMonth.month.length 
    const dateAsString = `${this.state.currentMonth.month} ${date}, 2019`
    const monthAsNumber: number = new Date(dateAsString).getMonth()
    const dateAsNumber = new Date(dateAsString).getDate()
    const dateObject = {
      dateAsString: dateAsString, 
      dateLength: currentMonthLength, 
      monthAsNumber: monthAsNumber, 
      currentMonth: this.state.currentMonth.month,
      dateAsNumber: dateAsNumber
    }
    const dates: Array<number> = this.state.selectedDates.concat(dateObject)
    const duplicateDate = _.find(this.state.selectedDates, {"dateAsString": dateAsString} as {})
    if(_.includes(this.state.selectedDates, duplicateDate)){
      const removedDate = _.pull(this.state.selectedDates, duplicateDate)
      this._sortSelectedDates()
      this.setState(function(){
        if(this.state.selectedDates.length > 1){
          return{
            selectedDates: removedDate,
            departureDate: this._sortSelectedDates()[0].dateAsString,
            arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
          }
        } else if(this.state.selectedDates.length < 1){
          return{
            departureDate: '',
            arrivalDate: '',
          }
        } else {
          return{
            departureDate: this._sortSelectedDates()[0].dateAsString,
            arrivalDate: '',
          }
        }
      })
    } else {
      this.setState(function(){
        return{
          selectedDates: dates
        }
      })
    }
  }

  _prevMonth = () => {
    if(this.state.date >= 1){
      this.setState(function(){
        return{
          date: this.state.date - 1
        }
      })
    }
  }

  _nextMonth = () => {
    if(this.state.date <= 10){
      this.setState(function(){
        return{
          date: this.state.date + 1
        }
      })
    }
  }

  _firstDayTheMonthDay = () => {
    const currentMonth = this.state.currentMonth.month 
    const firstDay     = new Date(`${currentMonth} 1, 2019`).toString().substring(0,3) 

    return firstDay
  }

  _formatDates = (dates: Array<number>) => {
    const array = []
    var countLoop = 0
    var count = 0 

    for(var x=0;x<dates.length;x++){
      array.push({"month": this.state.currentMonth.month, "day": dates[x]})
    }

    switch(this.state.currentMonth.month){
      case 'January' : 
        countLoop = 2
        break
      case 'February' : 
        countLoop = 2
        break  
      case 'March' : 
        countLoop = 6
        break  
      case 'April' : 
        countLoop = 4
        break  
      case 'May' : 
        countLoop = 1
        break 
      case 'June' : 
        countLoop = 6
        break   
      case 'July' : 
        countLoop = 3
        break  
      case 'August' : 
        countLoop = 0
        break
      case 'September' : 
        countLoop = 5
        break 
      case 'October' : 
        countLoop = 2
        break   
      case 'November' : 
        countLoop = 0
        break  
      case 'December' : 
        countLoop = 4
        break  
    }

    while(countLoop !== 0){ 
      array.push({key: "", empty: true})
      countLoop--
    }

    switch(this._firstDayTheMonthDay()){
      case 'Tue' :
        count = count + 1
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break 
      case 'Wed' :
        count = count + 2
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break
      case 'Thu' :
        count = count + 3
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break
      case 'Fri' :
        count = count + 4
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break
      case 'Sat' :
        count = count + 5
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break
      case 'Mon' :
        count = count + 0
        while(count !== -1){
          array.unshift({key: `blank-${count}`, empty: true}) 

          count = count - 1
        }
        break
      default:
        count =  count + 0  
        break
    } 
    
    return array as []
  }

  _hotelSelection = (itemValue: string) => {
    this.setState(function(){
      return{
        hotel: itemValue
      }
    })
  }

  _nameInput = (name: string) => {
    this.setState({name})
  }

  _validateFields = (name=this.state.name, departureDate=this.state.departureDate, arrivalDate=this.state.arrivalDate, hotelName=this.state.hotel) => {
    let whitespaceCharSearch                  = /\s/g;
    
    if(name !== "" && departureDate !== "" && arrivalDate !== "" && hotelName !== ""){
      if(name.length > 4){
        if(name.match(whitespaceCharSearch) !== null){
          return(
            this.setState(function(){
              return{
                bgColor: "blue",
                textColor: "white" 
              }
            })
          )  
        } 
      } 
    }
    return(
      this.setState(function(){
        return{
          bgColor: "#0b1c3a",
          textColor: "gray" 
        }
      })
    )    
      
  }

  render(){
    const dates    = _.range(1, parseInt(this.state.currentMonth.days, 10) + 1)
    const getMonth = this.state.currentMonth.month 

    return(
      <Booking
        getMonth={getMonth}
        _formatDates={this._formatDates(dates)}
        selectedDates={this.state.selectedDates}
        _selectDate={this._selectDate}
        _prevMonth={this._prevMonth}
        _nextMonth={this._nextMonth}
        arrivalDate={this.state.arrivalDate}
        departureDate={this.state.departureDate}
        hotel={this.state.hotel}
        _hotelSelection={this._hotelSelection}
        _nameInput={this._nameInput}
        name={this.state.name}
        _validateFields={this._validateFields}
        textColor={this.state.textColor}
        bgColor={this.state.bgColor}
      />
    )
  }
}

export default BookingsContainer;
