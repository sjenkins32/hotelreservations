import React, {Component} from 'react';
import {
  Text,
  Dimensions,
  View
} from 'react-native';

interface State {
  state?: Boolean 
}


class ErrorBoundary extends Component<State> {
  state = {
      hasError: false 
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <View>
        <Text 
          style={{alignSelf:'center', marginTop: Dimensions.get('window').height * .5, fontSize: 30}}>Something went wrong.
        </Text>
        <Text 
          style={{alignSelf:'center', fontSize: 30, color: 'blue'}}>
            Try Again
        </Text>
      </View>
    }

    return this.props.children; 
  }
}

export default ErrorBoundary;
