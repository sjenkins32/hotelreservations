import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeComponent from '../components/class/Home';
import BookingsComponent from '../components/container/BookingsContainer';

const Home = createStackNavigator({
  Home: HomeComponent,
});

const Bookings = createStackNavigator({
  Bookings: BookingsComponent,
});

export default createBottomTabNavigator({
    Home,
    Bookings
  },
  {
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      style: {
        backgroundColor: 'white'
      }
    }
  }
);
