"use strict";
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 *
 * By Sherrod Jenkins
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Bookings_1 = __importDefault(require("../class/Bookings"));
const lodash_1 = __importDefault(require("lodash"));
class BookingsContainer extends react_1.Component {
    constructor() {
        /*
          Initial State
        */
        super(...arguments);
        this.state = {
            inactiveDateColor: 'white',
            dates: [],
            hotel: '',
            name: '',
            selectedDates: [],
            arrivalDate: "",
            departureDate: "",
            date: new Date().getMonth(),
            fullDate: new Date().toString().substring(0, 3),
            day: new Date().getDate(),
            currentMonth: {},
            formatDatesArray: [],
            numOfRows: 7,
            numberOfExecutions: 0,
            removeDate: false,
            dateChange: false,
            months: [
                { "month": "January", "days": "31" },
                { "month": "February", "days": "28" },
                { "month": "March", "days": "31" },
                { "month": "April", "days": "30" },
                { "month": "May", "days": "31" },
                { "month": "June", "days": "30" },
                { "month": "July", "days": "31" },
                { "month": "August", "days": "31" },
                { "month": "September", "days": "30" },
                { "month": "October", "days": "31" },
                { "month": "November", "days": "30" },
                { "month": "December", "days": "31" }
            ],
            bgColor: '#0b1c3a',
            textColor: 'gray'
        };
        /*
          Functions
        */
        this._sortSelectedDates = () => {
            let sortedDayArray = lodash_1.default.sortBy(this.state.selectedDates, [function (a) { return a.dateAsNumber; }]);
            let sortedMonthArray = lodash_1.default.sortBy(sortedDayArray, [function (a) { return a.monthAsNumber; }]);
            return sortedMonthArray;
        };
        this._selectDate = (date) => {
            let currentMonthLength = this.state.currentMonth.month.length;
            const dateAsString = `${this.state.currentMonth.month} ${date}, 2019`;
            const monthAsNumber = new Date(dateAsString).getMonth();
            const dateAsNumber = new Date(dateAsString).getDate();
            const dateObject = {
                dateAsString: dateAsString,
                dateLength: currentMonthLength,
                monthAsNumber: monthAsNumber,
                currentMonth: this.state.currentMonth.month,
                dateAsNumber: dateAsNumber
            };
            const dates = this.state.selectedDates.concat(dateObject);
            const duplicateDate = lodash_1.default.find(this.state.selectedDates, { "dateAsString": dateAsString });
            if (lodash_1.default.includes(this.state.selectedDates, duplicateDate)) {
                const removedDate = lodash_1.default.pull(this.state.selectedDates, duplicateDate);
                this._sortSelectedDates();
                this.setState(function () {
                    if (this.state.selectedDates.length > 1) {
                        return {
                            selectedDates: removedDate,
                            departureDate: this._sortSelectedDates()[0].dateAsString,
                            arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
                        };
                    }
                    else if (this.state.selectedDates.length < 1) {
                        return {
                            departureDate: '',
                            arrivalDate: '',
                        };
                    }
                    else {
                        return {
                            departureDate: this._sortSelectedDates()[0].dateAsString,
                            arrivalDate: '',
                        };
                    }
                });
            }
            else {
                this.setState(function () {
                    return {
                        selectedDates: dates
                    };
                });
            }
        };
        this._prevMonth = () => {
            if (this.state.date >= 1) {
                this.setState(function () {
                    return {
                        date: this.state.date - 1
                    };
                });
            }
        };
        this._nextMonth = () => {
            if (this.state.date <= 10) {
                this.setState(function () {
                    return {
                        date: this.state.date + 1
                    };
                });
            }
        };
        this._firstDayTheMonthDay = () => {
            const currentMonth = this.state.currentMonth.month;
            const firstDay = new Date(`${currentMonth} 1, 2019`).toString().substring(0, 3);
            return firstDay;
        };
        this._formatDates = (count = 0, dates) => {
            const array = [];
            var countLoop = 0;
            for (var x = 0; x < dates.length; x++) {
                array.push({ "month": this.state.currentMonth.month, "day": dates[x] });
            }
            switch (this.state.currentMonth.month) {
                case 'January':
                    countLoop = 2;
                    break;
                case 'February':
                    countLoop = 2;
                    break;
                case 'March':
                    countLoop = 6;
                    break;
                case 'April':
                    countLoop = 4;
                    break;
                case 'May':
                    countLoop = 1;
                    break;
                case 'June':
                    countLoop = 6;
                    break;
                case 'July':
                    countLoop = 3;
                    break;
                case 'August':
                    countLoop = 0;
                    break;
                case 'September':
                    countLoop = 5;
                    break;
                case 'October':
                    countLoop = 2;
                    break;
                case 'November':
                    countLoop = 0;
                    break;
                case 'December':
                    countLoop = 4;
                    break;
            }
            while (countLoop !== 0) {
                array.push({ key: "", empty: true });
                countLoop--;
            }
            switch (this._firstDayTheMonthDay()) {
                case 'Tue':
                    count = count + 1;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Wed':
                    count = count + 2;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Thu':
                    count = count + 3;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Fri':
                    count = count + 4;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Sat':
                    count = count + 5;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Mon':
                    count = count + 0;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                default:
                    count = count + 0;
                    break;
            }
            return array;
        };
        this._hotelSelection = (itemValue) => {
            this.setState(function () {
                return {
                    hotel: itemValue
                };
            });
        };
        this._nameInput = (name) => {
            this.setState({ name });
        };
        this._validateFields = (name = this.state.name, departureDate = this.state.departureDate, arrivalDate = this.state.arrivalDate, hotelName = this.state.hotel) => {
            let whitespaceCharSearch = /\s/g;
            if (name !== "" && departureDate !== "" && arrivalDate !== "" && hotelName !== "") {
                if (name.length > 4) {
                    if (name.match(whitespaceCharSearch) !== null) {
                        return (this.setState(function () {
                            return {
                                bgColor: "blue",
                                textColor: "white"
                            };
                        }));
                    }
                }
            }
            return (this.setState(function () {
                return {
                    bgColor: "#0b1c3a",
                    textColor: "gray"
                };
            }));
        };
    }
    /*
      Lifecycle Methods
    */
    componentDidMount() {
        this.setState(function () {
            return {
                currentMonth: this.state.months[this.state.date]
            };
        });
    }
    componentDidUpdate(prevProps, prevState) {
        if (!lodash_1.default.isEqual(this.state.selectedDates, prevState.selectedDates)) {
            this._sortSelectedDates();
            this.setState(function () {
                if (this.state.selectedDates.length > 1) {
                    return {
                        departureDate: this._sortSelectedDates()[0].dateAsString,
                        arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
                    };
                }
                else {
                    return {
                        departureDate: this._sortSelectedDates()[0].dateAsString,
                    };
                }
            });
        }
        else if (!lodash_1.default.isEqual(this.state.date, prevState.date)) {
            this.setState(function () {
                return {
                    currentMonth: this.state.months[this.state.date]
                };
            });
        }
        else if (!lodash_1.default.isEqual(this.state.name, prevState.name)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.hotel, prevState.hotel)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.departureDate, prevState.departureDate)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.arrivalDate, prevState.arrivalDate)) {
            this._validateFields();
        }
    }
    render() {
        const dates = lodash_1.default.range(1, parseInt(this.state.currentMonth.days, 10) + 1);
        const getMonth = this.state.currentMonth.month;
        return (<Bookings_1.default getMonth={getMonth} _formatDates={this._formatDates(0, dates)} selectedDates={this.state.selectedDates} _selectDate={this._selectDate} _prevMonth={this._prevMonth} _nextMonth={this._nextMonth} arrivalDate={this.state.arrivalDate} departureDate={this.state.departureDate} hotel={this.state.hotel} _hotelSelection={this._hotelSelection} _nameInput={this._nameInput} name={this.state.name} _validateFields={this._validateFields} textColor={this.state.textColor} bgColor={this.state.bgColor}/>);
    }
}
/*
  Navigation Config
*/
BookingsContainer.navigationOptions = {
    title: 'Bookings',
    headerStyle: {
        backgroundColor: 'white'
    },
    headerTitleStyle: {
        color: 'gray',
        fontSize: 20
    },
};
exports.default = BookingsContainer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL1VzZXJzL3NoZXJyb2RqZW5raW5zL2NoYWxsZW5nZS9ob3RlbHJlc2VydmF0aW9ucy9jb21wb25lbnRzL2NvbnRhaW5lci9Cb29raW5nc0NvbnRhaW5lci50c3giLCJtYXBwaW5ncyI6IjtBQUFBOzs7Ozs7Ozs7R0FTRzs7Ozs7Ozs7Ozs7O0FBRUgsK0NBQXlDO0FBQ3pDLGlFQUF3QztBQUN4QyxvREFBdUI7QUEwQnZCLE1BQU0saUJBQWtCLFNBQVEsaUJBQWdCO0lBQWhEO1FBQ0U7O1VBRUU7O1FBRUYsVUFBSyxHQUFHO1lBQ04saUJBQWlCLEVBQUUsT0FBTztZQUMxQixLQUFLLEVBQUUsRUFBRTtZQUNULEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtZQUNmLGFBQWEsRUFBRSxFQUFFO1lBQ2pCLElBQUksRUFBRSxJQUFJLElBQUksRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMzQixRQUFRLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQztZQUM5QyxHQUFHLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFDekIsWUFBWSxFQUFFLEVBQWtCO1lBQ2hDLGdCQUFnQixFQUFFLEVBQUU7WUFDcEIsU0FBUyxFQUFFLENBQUM7WUFDWixrQkFBa0IsRUFBRSxDQUFDO1lBQ3JCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE1BQU0sRUFBRTtnQkFDTixFQUFDLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDakMsRUFBQyxPQUFPLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2xDLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUMvQixFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDL0IsRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQzdCLEVBQUMsT0FBTyxFQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUM5QixFQUFDLE9BQU8sRUFBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDOUIsRUFBQyxPQUFPLEVBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2hDLEVBQUMsT0FBTyxFQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUNuQyxFQUFDLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDakMsRUFBQyxPQUFPLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2xDLEVBQUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2FBQ25DO1lBQ0QsT0FBTyxFQUFFLFNBQVM7WUFDbEIsU0FBUyxFQUFFLE1BQU07U0FDbEIsQ0FBQTtRQTJERDs7VUFFRTtRQUNGLHVCQUFrQixHQUFHLEdBQUcsRUFBRTtZQUN4QixJQUFJLGNBQWMsR0FBRyxnQkFBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDLFVBQVMsQ0FBQyxJQUFHLE9BQU8sQ0FBQyxDQUFDLFlBQVksQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDOUYsSUFBSSxnQkFBZ0IsR0FBRyxnQkFBQyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFTLENBQUMsSUFBRyxPQUFPLENBQUMsQ0FBQyxhQUFhLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRXZGLE9BQU8sZ0JBQWdCLENBQUE7UUFDekIsQ0FBQyxDQUFBO1FBRUQsZ0JBQVcsR0FBRyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQzdCLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQTtZQUM3RCxNQUFNLFlBQVksR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxJQUFJLFFBQVEsQ0FBQTtZQUNyRSxNQUFNLGFBQWEsR0FBVyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUMvRCxNQUFNLFlBQVksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtZQUNyRCxNQUFNLFVBQVUsR0FBRztnQkFDakIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFVBQVUsRUFBRSxrQkFBa0I7Z0JBQzlCLGFBQWEsRUFBRSxhQUFhO2dCQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSztnQkFDM0MsWUFBWSxFQUFFLFlBQVk7YUFDM0IsQ0FBQTtZQUNELE1BQU0sS0FBSyxHQUFrQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDeEUsTUFBTSxhQUFhLEdBQUcsZ0JBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsRUFBQyxjQUFjLEVBQUUsWUFBWSxFQUFPLENBQUMsQ0FBQTtZQUM1RixJQUFHLGdCQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxFQUFDO2dCQUNyRCxNQUFNLFdBQVcsR0FBRyxnQkFBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQTtnQkFDbkUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7Z0JBQ3pCLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO3dCQUNyQyxPQUFNOzRCQUNKLGFBQWEsRUFBRSxXQUFXOzRCQUMxQixhQUFhLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTs0QkFDeEQsV0FBVyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxZQUFZO3lCQUMxRixDQUFBO3FCQUNGO3lCQUFNLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQzt3QkFDNUMsT0FBTTs0QkFDSixhQUFhLEVBQUUsRUFBRTs0QkFDakIsV0FBVyxFQUFFLEVBQUU7eUJBQ2hCLENBQUE7cUJBQ0Y7eUJBQU07d0JBQ0wsT0FBTTs0QkFDSixhQUFhLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTs0QkFDeEQsV0FBVyxFQUFFLEVBQUU7eUJBQ2hCLENBQUE7cUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUNaLE9BQU07d0JBQ0osYUFBYSxFQUFFLEtBQUs7cUJBQ3JCLENBQUE7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtRQUNILENBQUMsQ0FBQTtRQUVELGVBQVUsR0FBRyxHQUFHLEVBQUU7WUFDaEIsSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUM7Z0JBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osT0FBTTt3QkFDSixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQztxQkFDMUIsQ0FBQTtnQkFDSCxDQUFDLENBQUMsQ0FBQTthQUNIO1FBQ0gsQ0FBQyxDQUFBO1FBRUQsZUFBVSxHQUFHLEdBQUcsRUFBRTtZQUNoQixJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBQztnQkFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQztvQkFDWixPQUFNO3dCQUNKLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDO3FCQUMxQixDQUFBO2dCQUNILENBQUMsQ0FBQyxDQUFBO2FBQ0g7UUFDSCxDQUFDLENBQUE7UUFFRCx5QkFBb0IsR0FBRyxHQUFHLEVBQUU7WUFDMUIsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFBO1lBQ2xELE1BQU0sUUFBUSxHQUFPLElBQUksSUFBSSxDQUFDLEdBQUcsWUFBWSxVQUFVLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFBO1lBRWxGLE9BQU8sUUFBUSxDQUFBO1FBQ2pCLENBQUMsQ0FBQTtRQUVELGlCQUFZLEdBQUcsQ0FBQyxRQUFnQixDQUFDLEVBQUUsS0FBb0IsRUFBRSxFQUFFO1lBQ3pELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQTtZQUNoQixJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUE7WUFFakIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUMsQ0FBQyxFQUFFLEVBQUM7Z0JBQzdCLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBO2FBQ3RFO1lBRUQsUUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUM7Z0JBQ25DLEtBQUssU0FBUztvQkFDWixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxVQUFVO29CQUNiLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLE9BQU87b0JBQ1YsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssT0FBTztvQkFDVixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxLQUFLO29CQUNSLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLE1BQU07b0JBQ1QsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssTUFBTTtvQkFDVCxTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxRQUFRO29CQUNYLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLFdBQVc7b0JBQ2QsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssU0FBUztvQkFDWixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxVQUFVO29CQUNiLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLFVBQVU7b0JBQ2IsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2FBQ1I7WUFFRCxPQUFNLFNBQVMsS0FBSyxDQUFDLEVBQUM7Z0JBQ3BCLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO2dCQUNsQyxTQUFTLEVBQUUsQ0FBQTthQUNaO1lBRUQsUUFBTyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBQztnQkFDakMsS0FBSyxLQUFLO29CQUNSLEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO29CQUNqQixPQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBQzt3QkFDakIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO3dCQUVuRCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtxQkFDbEI7b0JBQ0QsTUFBSztnQkFDUCxLQUFLLEtBQUs7b0JBQ1IsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2pCLE9BQU0sS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFDO3dCQUNqQixLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUMsR0FBRyxFQUFFLFNBQVMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBRW5ELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO3FCQUNsQjtvQkFDRCxNQUFLO2dCQUNQLEtBQUssS0FBSztvQkFDUixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtvQkFDakIsT0FBTSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUM7d0JBQ2pCLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUUsU0FBUyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFFbkQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7cUJBQ2xCO29CQUNELE1BQUs7Z0JBQ1AsS0FBSyxLQUFLO29CQUNSLEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO29CQUNqQixPQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBQzt3QkFDakIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO3dCQUVuRCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtxQkFDbEI7b0JBQ0QsTUFBSztnQkFDUCxLQUFLLEtBQUs7b0JBQ1IsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2pCLE9BQU0sS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFDO3dCQUNqQixLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUMsR0FBRyxFQUFFLFNBQVMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBRW5ELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO3FCQUNsQjtvQkFDRCxNQUFLO2dCQUNQLEtBQUssS0FBSztvQkFDUixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtvQkFDakIsT0FBTSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUM7d0JBQ2pCLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUUsU0FBUyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFFbkQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7cUJBQ2xCO29CQUNELE1BQUs7Z0JBQ1A7b0JBQ0UsS0FBSyxHQUFJLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2xCLE1BQUs7YUFDUjtZQUVELE9BQU8sS0FBVyxDQUFBO1FBQ3BCLENBQUMsQ0FBQTtRQUVELG9CQUFlLEdBQUcsQ0FBQyxTQUFpQixFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDWixPQUFNO29CQUNKLEtBQUssRUFBRSxTQUFTO2lCQUNqQixDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQUE7UUFDSixDQUFDLENBQUE7UUFFRCxlQUFVLEdBQUcsQ0FBQyxJQUFZLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQTtRQUN2QixDQUFDLENBQUE7UUFFRCxvQkFBZSxHQUFHLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLGFBQWEsR0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxXQUFXLEdBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsU0FBUyxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDakosSUFBSSxvQkFBb0IsR0FBb0IsS0FBSyxDQUFDO1lBRWxELElBQUcsSUFBSSxLQUFLLEVBQUUsSUFBSSxhQUFhLEtBQUssRUFBRSxJQUFJLFdBQVcsS0FBSyxFQUFFLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBQztnQkFDL0UsSUFBRyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQztvQkFDakIsSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLEtBQUssSUFBSSxFQUFDO3dCQUMzQyxPQUFNLENBQ0osSUFBSSxDQUFDLFFBQVEsQ0FBQzs0QkFDWixPQUFNO2dDQUNKLE9BQU8sRUFBRSxNQUFNO2dDQUNmLFNBQVMsRUFBRSxPQUFPOzZCQUNuQixDQUFBO3dCQUNILENBQUMsQ0FBQyxDQUNILENBQUE7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNELE9BQU0sQ0FDSixJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNaLE9BQU07b0JBQ0osT0FBTyxFQUFFLFNBQVM7b0JBQ2xCLFNBQVMsRUFBRSxNQUFNO2lCQUNsQixDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQ0gsQ0FBQTtRQUVILENBQUMsQ0FBQTtJQTBCSCxDQUFDO0lBMVNDOztNQUVFO0lBQ0YsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNaLE9BQU07Z0JBQ0osWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2FBQ2pELENBQUE7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsU0FBUztRQUNyQyxJQUFHLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFDO1lBQy9ELElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBO1lBQ3pCLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO29CQUNyQyxPQUFNO3dCQUNKLGFBQWEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZO3dCQUN4RCxXQUFXLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLFlBQVk7cUJBQzFGLENBQUE7aUJBQ0Y7cUJBQU07b0JBQ0wsT0FBTTt3QkFDSixhQUFhLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTtxQkFDekQsQ0FBQTtpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFBO1NBQ0g7YUFBTSxJQUFHLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFDO1lBQ3BELElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osT0FBTTtvQkFDSixZQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7aUJBQ2pELENBQUE7WUFDSCxDQUFDLENBQUMsQ0FBQTtTQUNIO2FBQU0sSUFBRyxDQUFDLGdCQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBQztZQUNsRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDekI7YUFBTSxJQUFHLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFDO1lBQ3RELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN2QjthQUFNLElBQUcsQ0FBQyxnQkFBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUM7WUFDdEUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1NBQ3ZCO2FBQU0sSUFBRyxDQUFDLGdCQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBQztZQUNsRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDdkI7SUFDSCxDQUFDO0lBeU9ELE1BQU07UUFDSixNQUFNLEtBQUssR0FBTSxnQkFBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUMzRSxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUE7UUFFOUMsT0FBTSxDQUNKLENBQUMsa0JBQU8sQ0FDTixRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FDbkIsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FDMUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FDeEMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUM5QixVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQzVCLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FDNUIsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FDcEMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FDeEMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FDeEIsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUN0QyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQzVCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQ3RCLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FDdEMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FDaEMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFDNUIsQ0FDSCxDQUFBO0lBQ0gsQ0FBQzs7QUF2VEQ7O0VBRUU7QUFDSyxtQ0FBaUIsR0FBRztJQUN6QixLQUFLLEVBQUUsVUFBVTtJQUNqQixXQUFXLEVBQUU7UUFDWCxlQUFlLEVBQUUsT0FBTztLQUN6QjtJQUNELGdCQUFnQixFQUFFO1FBQ2hCLEtBQUssRUFBRSxNQUFNO1FBQ2IsUUFBUSxFQUFFLEVBQUU7S0FDYjtDQUNGLENBQUM7QUE4U0osa0JBQWUsaUJBQWlCLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiL1VzZXJzL3NoZXJyb2RqZW5raW5zL2NoYWxsZW5nZS9ob3RlbHJlc2VydmF0aW9ucy9jb21wb25lbnRzL2NvbnRhaW5lci9Cb29raW5nc0NvbnRhaW5lci50c3giXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBTYW1wbGUgUmVhY3QgTmF0aXZlIEFwcFxuICogaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL3JlYWN0LW5hdGl2ZVxuICpcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKiBAbGludC1pZ25vcmUtZXZlcnkgWFBMQVRKU0NPUFlSSUdIVDFcbiAqIFxuICogQnkgU2hlcnJvZCBKZW5raW5zXG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBCb29raW5nIGZyb20gJy4uL2NsYXNzL0Jvb2tpbmdzJztcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5cbmludGVyZmFjZSBTdGF0ZSB7XG4gIGluYWN0aXZlRGF0ZUNvbG9yPzogc3RyaW5nLFxuICBob3RlbD86IHN0cmluZywgXG4gIG5hbWU/OiBzdHJpbmcsXG4gIGFycml2YWxEYXRlPzogc3RyaW5nLFxuICBkZXBhcnR1cmVEYXRlPzogc3RyaW5nLFxuICBkYXRlPzogbnVtYmVyLFxuICBmdWxsRGF0ZT86IHN0cmluZyxcbiAgZGF5PzogbnVtYmVyLFxuICB0b3VjaGVkPzogQm9vbGVhbixcbiAgbnVtYmVyT2ZSb3dzPzogbnVtYmVyLFxuICBkYXRlQ2hhbmdlPzogQm9vbGVhbixcbiAgY3VycmVudE1vbnRoPzogc3RyaW5nW11cbn1cblxuaW50ZXJmYWNlIGN1cnJlbnRNb250aCB7XG4gIG1vbnRoOiBzdHJpbmcsXG4gIGRheXM6IHN0cmluZ1xufVxuXG5pbnRlcmZhY2Ugc2VsZWN0ZWREYXRlcyB7XG5cbn1cblxuY2xhc3MgQm9va2luZ3NDb250YWluZXIgZXh0ZW5kcyBDb21wb25lbnQ8U3RhdGU+IHtcbiAgLypcbiAgICBJbml0aWFsIFN0YXRlXG4gICovXG5cbiAgc3RhdGUgPSB7XG4gICAgaW5hY3RpdmVEYXRlQ29sb3I6ICd3aGl0ZScsXG4gICAgZGF0ZXM6IFtdLFxuICAgIGhvdGVsOiAnJywgXG4gICAgbmFtZTogJycsXG4gICAgc2VsZWN0ZWREYXRlczogW10sXG4gICAgYXJyaXZhbERhdGU6IFwiXCIsXG4gICAgZGVwYXJ0dXJlRGF0ZTogXCJcIixcbiAgICBkYXRlOiBuZXcgRGF0ZSgpLmdldE1vbnRoKCksXG4gICAgZnVsbERhdGU6IG5ldyBEYXRlKCkudG9TdHJpbmcoKS5zdWJzdHJpbmcoMCwzKSxcbiAgICBkYXk6IG5ldyBEYXRlKCkuZ2V0RGF0ZSgpLFxuICAgIGN1cnJlbnRNb250aDoge30gYXMgY3VycmVudE1vbnRoLFxuICAgIGZvcm1hdERhdGVzQXJyYXk6IFtdLFxuICAgIG51bU9mUm93czogNyxcbiAgICBudW1iZXJPZkV4ZWN1dGlvbnM6IDAsXG4gICAgcmVtb3ZlRGF0ZTogZmFsc2UsXG4gICAgZGF0ZUNoYW5nZTogZmFsc2UsXG4gICAgbW9udGhzOiBbXG4gICAgICB7XCJtb250aFwiOlwiSmFudWFyeVwiLCBcImRheXNcIjogXCIzMVwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJGZWJydWFyeVwiLCBcImRheXNcIjogXCIyOFwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJNYXJjaFwiLCBcImRheXNcIjogXCIzMVwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJBcHJpbFwiLCBcImRheXNcIjogXCIzMFwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJNYXlcIiwgXCJkYXlzXCI6IFwiMzFcIn0sIFxuICAgICAge1wibW9udGhcIjpcIkp1bmVcIiwgXCJkYXlzXCI6IFwiMzBcIn0sIFxuICAgICAge1wibW9udGhcIjpcIkp1bHlcIiwgXCJkYXlzXCI6IFwiMzFcIn0sXG4gICAgICB7XCJtb250aFwiOlwiQXVndXN0XCIsIFwiZGF5c1wiOiBcIjMxXCJ9LFxuICAgICAge1wibW9udGhcIjpcIlNlcHRlbWJlclwiLCBcImRheXNcIjogXCIzMFwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJPY3RvYmVyXCIsIFwiZGF5c1wiOiBcIjMxXCJ9LFxuICAgICAge1wibW9udGhcIjpcIk5vdmVtYmVyXCIsIFwiZGF5c1wiOiBcIjMwXCJ9LFxuICAgICAge1wibW9udGhcIjpcIkRlY2VtYmVyXCIsIFwiZGF5c1wiOiBcIjMxXCJ9XG4gICAgXSwgXG4gICAgYmdDb2xvcjogJyMwYjFjM2EnLFxuICAgIHRleHRDb2xvcjogJ2dyYXknXG4gIH1cblxuICAvKlxuICAgIE5hdmlnYXRpb24gQ29uZmlnXG4gICovICBcbiAgc3RhdGljIG5hdmlnYXRpb25PcHRpb25zID0ge1xuICAgIHRpdGxlOiAnQm9va2luZ3MnLFxuICAgIGhlYWRlclN0eWxlOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6ICd3aGl0ZSdcbiAgICB9LFxuICAgIGhlYWRlclRpdGxlU3R5bGU6IHtcbiAgICAgIGNvbG9yOiAnZ3JheScsXG4gICAgICBmb250U2l6ZTogMjBcbiAgICB9LFxuICB9O1xuXG4gIC8qXG4gICAgTGlmZWN5Y2xlIE1ldGhvZHNcbiAgKi9cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgcmV0dXJue1xuICAgICAgICBjdXJyZW50TW9udGg6IHRoaXMuc3RhdGUubW9udGhzW3RoaXMuc3RhdGUuZGF0ZV1cbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcywgcHJldlN0YXRlKXtcbiAgICBpZighXy5pc0VxdWFsKHRoaXMuc3RhdGUuc2VsZWN0ZWREYXRlcywgcHJldlN0YXRlLnNlbGVjdGVkRGF0ZXMpKXtcbiAgICAgIHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClcbiAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgaWYodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLmxlbmd0aCA+IDEpe1xuICAgICAgICAgIHJldHVybnsgXG4gICAgICAgICAgICBkZXBhcnR1cmVEYXRlOiB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpWzBdLmRhdGVBc1N0cmluZyxcbiAgICAgICAgICAgIGFycml2YWxEYXRlOiB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpW3RoaXMuX3NvcnRTZWxlY3RlZERhdGVzKCkubGVuZ3RoIC0gMV0uZGF0ZUFzU3RyaW5nXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybntcbiAgICAgICAgICAgIGRlcGFydHVyZURhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbMF0uZGF0ZUFzU3RyaW5nLFxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9IGVsc2UgaWYoIV8uaXNFcXVhbCh0aGlzLnN0YXRlLmRhdGUsIHByZXZTdGF0ZS5kYXRlKSl7XG4gICAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybntcbiAgICAgICAgICBjdXJyZW50TW9udGg6IHRoaXMuc3RhdGUubW9udGhzW3RoaXMuc3RhdGUuZGF0ZV1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9IGVsc2UgaWYoIV8uaXNFcXVhbCh0aGlzLnN0YXRlLm5hbWUsIHByZXZTdGF0ZS5uYW1lKSl7XG4gICAgICAgIHRoaXMuX3ZhbGlkYXRlRmllbGRzKClcbiAgICB9IGVsc2UgaWYoIV8uaXNFcXVhbCh0aGlzLnN0YXRlLmhvdGVsLCBwcmV2U3RhdGUuaG90ZWwpKXtcbiAgICAgIHRoaXMuX3ZhbGlkYXRlRmllbGRzKClcbiAgICB9IGVsc2UgaWYoIV8uaXNFcXVhbCh0aGlzLnN0YXRlLmRlcGFydHVyZURhdGUsIHByZXZTdGF0ZS5kZXBhcnR1cmVEYXRlKSl7XG4gICAgICB0aGlzLl92YWxpZGF0ZUZpZWxkcygpXG4gICAgfSBlbHNlIGlmKCFfLmlzRXF1YWwodGhpcy5zdGF0ZS5hcnJpdmFsRGF0ZSwgcHJldlN0YXRlLmFycml2YWxEYXRlKSl7XG4gICAgICB0aGlzLl92YWxpZGF0ZUZpZWxkcygpXG4gICAgfSAgICAgXG4gIH1cblxuICAvKlxuICAgIEZ1bmN0aW9uc1xuICAqL1xuICBfc29ydFNlbGVjdGVkRGF0ZXMgPSAoKSA9PiB7XG4gICAgbGV0IHNvcnRlZERheUFycmF5ID0gXy5zb3J0QnkodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLCBbZnVuY3Rpb24oYSl7IHJldHVybiBhLmRhdGVBc051bWJlcn1dKVxuICAgIGxldCBzb3J0ZWRNb250aEFycmF5ID0gXy5zb3J0Qnkoc29ydGVkRGF5QXJyYXksIFtmdW5jdGlvbihhKXsgcmV0dXJuIGEubW9udGhBc051bWJlcn1dKVxuXG4gICAgcmV0dXJuIHNvcnRlZE1vbnRoQXJyYXlcbiAgfVxuXG4gIF9zZWxlY3REYXRlID0gKGRhdGU6IG51bWJlcikgPT4ge1xuICAgIGxldCBjdXJyZW50TW9udGhMZW5ndGggPSB0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aC5sZW5ndGggXG4gICAgY29uc3QgZGF0ZUFzU3RyaW5nID0gYCR7dGhpcy5zdGF0ZS5jdXJyZW50TW9udGgubW9udGh9ICR7ZGF0ZX0sIDIwMTlgXG4gICAgY29uc3QgbW9udGhBc051bWJlcjogbnVtYmVyID0gbmV3IERhdGUoZGF0ZUFzU3RyaW5nKS5nZXRNb250aCgpXG4gICAgY29uc3QgZGF0ZUFzTnVtYmVyID0gbmV3IERhdGUoZGF0ZUFzU3RyaW5nKS5nZXREYXRlKClcbiAgICBjb25zdCBkYXRlT2JqZWN0ID0ge1xuICAgICAgZGF0ZUFzU3RyaW5nOiBkYXRlQXNTdHJpbmcsIFxuICAgICAgZGF0ZUxlbmd0aDogY3VycmVudE1vbnRoTGVuZ3RoLCBcbiAgICAgIG1vbnRoQXNOdW1iZXI6IG1vbnRoQXNOdW1iZXIsIFxuICAgICAgY3VycmVudE1vbnRoOiB0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aCxcbiAgICAgIGRhdGVBc051bWJlcjogZGF0ZUFzTnVtYmVyXG4gICAgfVxuICAgIGNvbnN0IGRhdGVzOiBBcnJheTxudW1iZXI+ID0gdGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLmNvbmNhdChkYXRlT2JqZWN0KVxuICAgIGNvbnN0IGR1cGxpY2F0ZURhdGUgPSBfLmZpbmQodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLCB7XCJkYXRlQXNTdHJpbmdcIjogZGF0ZUFzU3RyaW5nfSBhcyB7fSlcbiAgICBpZihfLmluY2x1ZGVzKHRoaXMuc3RhdGUuc2VsZWN0ZWREYXRlcywgZHVwbGljYXRlRGF0ZSkpe1xuICAgICAgY29uc3QgcmVtb3ZlZERhdGUgPSBfLnB1bGwodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLCBkdXBsaWNhdGVEYXRlKVxuICAgICAgdGhpcy5fc29ydFNlbGVjdGVkRGF0ZXMoKVxuICAgICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgICBpZih0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMubGVuZ3RoID4gMSl7XG4gICAgICAgICAgcmV0dXJue1xuICAgICAgICAgICAgc2VsZWN0ZWREYXRlczogcmVtb3ZlZERhdGUsXG4gICAgICAgICAgICBkZXBhcnR1cmVEYXRlOiB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpWzBdLmRhdGVBc1N0cmluZyxcbiAgICAgICAgICAgIGFycml2YWxEYXRlOiB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpW3RoaXMuX3NvcnRTZWxlY3RlZERhdGVzKCkubGVuZ3RoIC0gMV0uZGF0ZUFzU3RyaW5nXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLmxlbmd0aCA8IDEpe1xuICAgICAgICAgIHJldHVybntcbiAgICAgICAgICAgIGRlcGFydHVyZURhdGU6ICcnLFxuICAgICAgICAgICAgYXJyaXZhbERhdGU6ICcnLFxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm57XG4gICAgICAgICAgICBkZXBhcnR1cmVEYXRlOiB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpWzBdLmRhdGVBc1N0cmluZyxcbiAgICAgICAgICAgIGFycml2YWxEYXRlOiAnJyxcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJue1xuICAgICAgICAgIHNlbGVjdGVkRGF0ZXM6IGRhdGVzXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgX3ByZXZNb250aCA9ICgpID0+IHtcbiAgICBpZih0aGlzLnN0YXRlLmRhdGUgPj0gMSl7XG4gICAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybntcbiAgICAgICAgICBkYXRlOiB0aGlzLnN0YXRlLmRhdGUgLSAxXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgX25leHRNb250aCA9ICgpID0+IHtcbiAgICBpZih0aGlzLnN0YXRlLmRhdGUgPD0gMTApe1xuICAgICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm57XG4gICAgICAgICAgZGF0ZTogdGhpcy5zdGF0ZS5kYXRlICsgMVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIF9maXJzdERheVRoZU1vbnRoRGF5ID0gKCkgPT4ge1xuICAgIGNvbnN0IGN1cnJlbnRNb250aCA9IHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoIFxuICAgIGNvbnN0IGZpcnN0RGF5ICAgICA9IG5ldyBEYXRlKGAke2N1cnJlbnRNb250aH0gMSwgMjAxOWApLnRvU3RyaW5nKCkuc3Vic3RyaW5nKDAsMykgXG5cbiAgICByZXR1cm4gZmlyc3REYXlcbiAgfVxuXG4gIF9mb3JtYXREYXRlcyA9IChjb3VudDogbnVtYmVyID0gMCwgZGF0ZXM6IEFycmF5PG51bWJlcj4pID0+IHtcbiAgICBjb25zdCBhcnJheSA9IFtdXG4gICAgdmFyIGNvdW50TG9vcCA9IDAgXG5cbiAgICBmb3IodmFyIHg9MDt4PGRhdGVzLmxlbmd0aDt4Kyspe1xuICAgICAgYXJyYXkucHVzaCh7XCJtb250aFwiOiB0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aCwgXCJkYXlcIjogZGF0ZXNbeF19KVxuICAgIH1cblxuICAgIHN3aXRjaCh0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aCl7XG4gICAgICBjYXNlICdKYW51YXJ5JyA6IFxuICAgICAgICBjb3VudExvb3AgPSAyXG4gICAgICAgIGJyZWFrXG4gICAgICBjYXNlICdGZWJydWFyeScgOiBcbiAgICAgICAgY291bnRMb29wID0gMlxuICAgICAgICBicmVhayAgXG4gICAgICBjYXNlICdNYXJjaCcgOiBcbiAgICAgICAgY291bnRMb29wID0gNlxuICAgICAgICBicmVhayAgXG4gICAgICBjYXNlICdBcHJpbCcgOiBcbiAgICAgICAgY291bnRMb29wID0gNFxuICAgICAgICBicmVhayAgXG4gICAgICBjYXNlICdNYXknIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDFcbiAgICAgICAgYnJlYWsgXG4gICAgICBjYXNlICdKdW5lJyA6IFxuICAgICAgICBjb3VudExvb3AgPSA2XG4gICAgICAgIGJyZWFrICAgXG4gICAgICBjYXNlICdKdWx5JyA6IFxuICAgICAgICBjb3VudExvb3AgPSAzXG4gICAgICAgIGJyZWFrICBcbiAgICAgIGNhc2UgJ0F1Z3VzdCcgOiBcbiAgICAgICAgY291bnRMb29wID0gMFxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnU2VwdGVtYmVyJyA6IFxuICAgICAgICBjb3VudExvb3AgPSA1XG4gICAgICAgIGJyZWFrIFxuICAgICAgY2FzZSAnT2N0b2JlcicgOiBcbiAgICAgICAgY291bnRMb29wID0gMlxuICAgICAgICBicmVhayAgIFxuICAgICAgY2FzZSAnTm92ZW1iZXInIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDBcbiAgICAgICAgYnJlYWsgIFxuICAgICAgY2FzZSAnRGVjZW1iZXInIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDRcbiAgICAgICAgYnJlYWsgIFxuICAgIH1cblxuICAgIHdoaWxlKGNvdW50TG9vcCAhPT0gMCl7IFxuICAgICAgYXJyYXkucHVzaCh7a2V5OiBcIlwiLCBlbXB0eTogdHJ1ZX0pXG4gICAgICBjb3VudExvb3AtLVxuICAgIH1cblxuICAgIHN3aXRjaCh0aGlzLl9maXJzdERheVRoZU1vbnRoRGF5KCkpe1xuICAgICAgY2FzZSAnVHVlJyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyAxXG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVhayBcbiAgICAgIGNhc2UgJ1dlZCcgOlxuICAgICAgICBjb3VudCA9IGNvdW50ICsgMlxuICAgICAgICB3aGlsZShjb3VudCAhPT0gLTEpe1xuICAgICAgICAgIGFycmF5LnVuc2hpZnQoe2tleTogYGJsYW5rLSR7Y291bnR9YCwgZW1wdHk6IHRydWV9KSBcblxuICAgICAgICAgIGNvdW50ID0gY291bnQgLSAxXG4gICAgICAgIH1cbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ1RodScgOlxuICAgICAgICBjb3VudCA9IGNvdW50ICsgM1xuICAgICAgICB3aGlsZShjb3VudCAhPT0gLTEpe1xuICAgICAgICAgIGFycmF5LnVuc2hpZnQoe2tleTogYGJsYW5rLSR7Y291bnR9YCwgZW1wdHk6IHRydWV9KSBcblxuICAgICAgICAgIGNvdW50ID0gY291bnQgLSAxXG4gICAgICAgIH1cbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ0ZyaScgOlxuICAgICAgICBjb3VudCA9IGNvdW50ICsgNFxuICAgICAgICB3aGlsZShjb3VudCAhPT0gLTEpe1xuICAgICAgICAgIGFycmF5LnVuc2hpZnQoe2tleTogYGJsYW5rLSR7Y291bnR9YCwgZW1wdHk6IHRydWV9KSBcblxuICAgICAgICAgIGNvdW50ID0gY291bnQgLSAxXG4gICAgICAgIH1cbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ1NhdCcgOlxuICAgICAgICBjb3VudCA9IGNvdW50ICsgNVxuICAgICAgICB3aGlsZShjb3VudCAhPT0gLTEpe1xuICAgICAgICAgIGFycmF5LnVuc2hpZnQoe2tleTogYGJsYW5rLSR7Y291bnR9YCwgZW1wdHk6IHRydWV9KSBcblxuICAgICAgICAgIGNvdW50ID0gY291bnQgLSAxXG4gICAgICAgIH1cbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ01vbicgOlxuICAgICAgICBjb3VudCA9IGNvdW50ICsgMFxuICAgICAgICB3aGlsZShjb3VudCAhPT0gLTEpe1xuICAgICAgICAgIGFycmF5LnVuc2hpZnQoe2tleTogYGJsYW5rLSR7Y291bnR9YCwgZW1wdHk6IHRydWV9KSBcblxuICAgICAgICAgIGNvdW50ID0gY291bnQgLSAxXG4gICAgICAgIH1cbiAgICAgICAgYnJlYWtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGNvdW50ID0gIGNvdW50ICsgMCAgXG4gICAgICAgIGJyZWFrXG4gICAgfSBcbiAgICBcbiAgICByZXR1cm4gYXJyYXkgYXMgW11cbiAgfVxuXG4gIF9ob3RlbFNlbGVjdGlvbiA9IChpdGVtVmFsdWU6IHN0cmluZykgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgIHJldHVybntcbiAgICAgICAgaG90ZWw6IGl0ZW1WYWx1ZVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICBfbmFtZUlucHV0ID0gKG5hbWU6IHN0cmluZykgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe25hbWV9KVxuICB9XG5cbiAgX3ZhbGlkYXRlRmllbGRzID0gKG5hbWU9dGhpcy5zdGF0ZS5uYW1lLCBkZXBhcnR1cmVEYXRlPXRoaXMuc3RhdGUuZGVwYXJ0dXJlRGF0ZSwgYXJyaXZhbERhdGU9dGhpcy5zdGF0ZS5hcnJpdmFsRGF0ZSwgaG90ZWxOYW1lPXRoaXMuc3RhdGUuaG90ZWwpID0+IHtcbiAgICBsZXQgd2hpdGVzcGFjZUNoYXJTZWFyY2ggICAgICAgICAgICAgICAgICA9IC9cXHMvZztcbiAgICBcbiAgICBpZihuYW1lICE9PSBcIlwiICYmIGRlcGFydHVyZURhdGUgIT09IFwiXCIgJiYgYXJyaXZhbERhdGUgIT09IFwiXCIgJiYgaG90ZWxOYW1lICE9PSBcIlwiKXtcbiAgICAgIGlmKG5hbWUubGVuZ3RoID4gNCl7XG4gICAgICAgIGlmKG5hbWUubWF0Y2god2hpdGVzcGFjZUNoYXJTZWFyY2gpICE9PSBudWxsKXtcbiAgICAgICAgICByZXR1cm4oXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgIHJldHVybntcbiAgICAgICAgICAgICAgICBiZ0NvbG9yOiBcImJsdWVcIixcbiAgICAgICAgICAgICAgICB0ZXh0Q29sb3I6IFwid2hpdGVcIiBcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICApICBcbiAgICAgICAgfSBcbiAgICAgIH0gXG4gICAgfVxuICAgIHJldHVybihcbiAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJue1xuICAgICAgICAgIGJnQ29sb3I6IFwiIzBiMWMzYVwiLFxuICAgICAgICAgIHRleHRDb2xvcjogXCJncmF5XCIgXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKSAgICBcbiAgICAgIFxuICB9XG5cbiAgcmVuZGVyKCl7XG4gICAgY29uc3QgZGF0ZXMgICAgPSBfLnJhbmdlKDEsIHBhcnNlSW50KHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLmRheXMsIDEwKSArIDEpXG4gICAgY29uc3QgZ2V0TW9udGggPSB0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aCBcblxuICAgIHJldHVybihcbiAgICAgIDxCb29raW5nXG4gICAgICAgIGdldE1vbnRoPXtnZXRNb250aH1cbiAgICAgICAgX2Zvcm1hdERhdGVzPXt0aGlzLl9mb3JtYXREYXRlcygwLCBkYXRlcyl9XG4gICAgICAgIHNlbGVjdGVkRGF0ZXM9e3RoaXMuc3RhdGUuc2VsZWN0ZWREYXRlc31cbiAgICAgICAgX3NlbGVjdERhdGU9e3RoaXMuX3NlbGVjdERhdGV9XG4gICAgICAgIF9wcmV2TW9udGg9e3RoaXMuX3ByZXZNb250aH1cbiAgICAgICAgX25leHRNb250aD17dGhpcy5fbmV4dE1vbnRofVxuICAgICAgICBhcnJpdmFsRGF0ZT17dGhpcy5zdGF0ZS5hcnJpdmFsRGF0ZX1cbiAgICAgICAgZGVwYXJ0dXJlRGF0ZT17dGhpcy5zdGF0ZS5kZXBhcnR1cmVEYXRlfVxuICAgICAgICBob3RlbD17dGhpcy5zdGF0ZS5ob3RlbH1cbiAgICAgICAgX2hvdGVsU2VsZWN0aW9uPXt0aGlzLl9ob3RlbFNlbGVjdGlvbn1cbiAgICAgICAgX25hbWVJbnB1dD17dGhpcy5fbmFtZUlucHV0fVxuICAgICAgICBuYW1lPXt0aGlzLnN0YXRlLm5hbWV9XG4gICAgICAgIF92YWxpZGF0ZUZpZWxkcz17dGhpcy5fdmFsaWRhdGVGaWVsZHN9XG4gICAgICAgIHRleHRDb2xvcj17dGhpcy5zdGF0ZS50ZXh0Q29sb3J9XG4gICAgICAgIGJnQ29sb3I9e3RoaXMuc3RhdGUuYmdDb2xvcn1cbiAgICAgIC8+XG4gICAgKVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEJvb2tpbmdzQ29udGFpbmVyO1xuIl0sInZlcnNpb24iOjN9