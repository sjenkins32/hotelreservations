"use strict";
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 *
 * By Sherrod Jenkins
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Bookings_1 = __importDefault(require("../class/Bookings"));
const lodash_1 = __importDefault(require("lodash"));
class BookingsContainer extends react_1.Component {
    constructor() {
        /*
          Initial State
        */
        super(...arguments);
        this.state = {
            inactiveDateColor: 'white',
            dates: [],
            hotel: '',
            name: '',
            selectedDates: [],
            arrivalDate: "",
            departureDate: "",
            date: new Date().getMonth(),
            fullDate: new Date().toString().substring(0, 3),
            day: new Date().getDate(),
            currentMonth: {},
            formatDatesArray: [],
            numOfRows: 7,
            numberOfExecutions: 0,
            removeDate: false,
            dateChange: false,
            months: [
                { "month": "January", "days": "31" },
                { "month": "February", "days": "28" },
                { "month": "March", "days": "31" },
                { "month": "April", "days": "30" },
                { "month": "May", "days": "31" },
                { "month": "June", "days": "30" },
                { "month": "July", "days": "31" },
                { "month": "August", "days": "31" },
                { "month": "September", "days": "30" },
                { "month": "October", "days": "31" },
                { "month": "November", "days": "30" },
                { "month": "December", "days": "31" }
            ],
            bgColor: '#0b1c3a',
            textColor: 'gray'
        };
        /*
          Functions
        */
        this._sortSelectedDates = () => {
            let sortedDayArray = lodash_1.default.sortBy(this.state.selectedDates, [function (a) { return a.dateAsNumber; }]);
            let sortedMonthArray = lodash_1.default.sortBy(sortedDayArray, [function (a) { return a.monthAsNumber; }]);
            return sortedMonthArray;
        };
        this._selectDate = (date) => {
            let currentMonthLength = this.state.currentMonth.month.length;
            const dateAsString = `${this.state.currentMonth.month} ${date}, 2019`;
            const monthAsNumber = new Date(dateAsString).getMonth();
            const dateAsNumber = new Date(dateAsString).getDate();
            const dateObject = {
                dateAsString: dateAsString,
                dateLength: currentMonthLength,
                monthAsNumber: monthAsNumber,
                currentMonth: this.state.currentMonth.month,
                dateAsNumber: dateAsNumber
            };
            const dates = this.state.selectedDates.concat(dateObject);
            const duplicateDate = lodash_1.default.find(this.state.selectedDates, { "dateAsString": dateAsString });
            if (lodash_1.default.includes(this.state.selectedDates, duplicateDate)) {
                const removedDate = lodash_1.default.pull(this.state.selectedDates, duplicateDate);
                this._sortSelectedDates();
                this.setState(function () {
                    if (this.state.selectedDates.length > 1) {
                        return {
                            selectedDates: removedDate,
                            departureDate: this._sortSelectedDates()[0].dateAsString,
                            arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
                        };
                    }
                    else if (this.state.selectedDates.length < 1) {
                        return {
                            departureDate: '',
                            arrivalDate: '',
                        };
                    }
                    else {
                        return {
                            departureDate: this._sortSelectedDates()[0].dateAsString,
                            arrivalDate: '',
                        };
                    }
                });
            }
            else {
                this.setState(function () {
                    return {
                        selectedDates: dates
                    };
                });
            }
        };
        this._prevMonth = () => {
            if (this.state.date >= 1) {
                this.setState(function () {
                    return {
                        date: this.state.date - 1
                    };
                });
            }
        };
        this._nextMonth = () => {
            if (this.state.date <= 10) {
                this.setState(function () {
                    return {
                        date: this.state.date + 1
                    };
                });
            }
        };
        this._firstDayTheMonthDay = () => {
            const currentMonth = this.state.currentMonth.month;
            const firstDay = new Date(`${currentMonth} 1, 2019`).toString().substring(0, 3);
            return firstDay;
        };
        this._formatDates = (count = 0, dates) => {
            const array = [];
            var countLoop = 0;
            for (var x = 0; x < dates.length; x++) {
                array.push({ "month": this.state.currentMonth.month, "day": dates[x] });
            }
            switch (this.state.currentMonth.month) {
                case 'January':
                    countLoop = 2;
                    break;
                case 'February':
                    countLoop = 2;
                    break;
                case 'March':
                    countLoop = 6;
                    break;
                case 'April':
                    countLoop = 4;
                    break;
                case 'May':
                    countLoop = 1;
                    break;
                case 'June':
                    countLoop = 6;
                    break;
                case 'July':
                    countLoop = 3;
                    break;
                case 'August':
                    countLoop = 0;
                    break;
                case 'September':
                    countLoop = 5;
                    break;
                case 'October':
                    countLoop = 2;
                    break;
                case 'November':
                    countLoop = 0;
                    break;
                case 'December':
                    countLoop = 4;
                    break;
            }
            while (countLoop !== 0) {
                array.push({ key: "", empty: true });
                countLoop--;
            }
            switch (this._firstDayTheMonthDay()) {
                case 'Tue':
                    count = count + 1;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Wed':
                    count = count + 2;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Thu':
                    count = count + 3;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Fri':
                    count = count + 4;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Sat':
                    count = count + 5;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                case 'Mon':
                    count = count + 0;
                    while (count !== -1) {
                        array.unshift({ key: `blank-${count}`, empty: true });
                        count = count - 1;
                    }
                    break;
                default:
                    count = count + 0;
                    break;
            }
            return array;
        };
        this._hotelSelection = (itemValue) => {
            this.setState(function () {
                return {
                    hotel: itemValue
                };
            });
        };
        this._nameInput = (name) => {
            this.setState({ name });
        };
        this._validateFields = (name = this.state.name, departureDate = this.state.departureDate, arrivalDate = this.state.arrivalDate, hotelName = this.state.hotel) => {
            let whitespaceCharSearch = /\s/g;
            if (name !== "" && departureDate !== "" && arrivalDate !== "" && hotelName !== "") {
                if (name.length > 4) {
                    if (name.match(whitespaceCharSearch) !== null) {
                        return (this.setState(function () {
                            return {
                                bgColor: "blue",
                                textColor: "white"
                            };
                        }));
                    }
                }
            }
            return (this.setState(function () {
                return {
                    bgColor: "#0b1c3a",
                    textColor: "gray"
                };
            }));
        };
    }
    /*
      Lifecycle Methods
    */
    componentDidMount() {
        this.setState(function () {
            return {
                currentMonth: this.state.months[this.state.date]
            };
        });
    }
    componentDidUpdate(prevProps, prevState) {
        if (!lodash_1.default.isEqual(this.state.selectedDates, prevState.selectedDates)) {
            this._sortSelectedDates();
            this.setState(function () {
                if (this.state.selectedDates.length > 1) {
                    return {
                        departureDate: this._sortSelectedDates()[0].dateAsString,
                        arrivalDate: this._sortSelectedDates()[this._sortSelectedDates().length - 1].dateAsString
                    };
                }
                else {
                    return {
                        departureDate: this._sortSelectedDates()[0].dateAsString,
                    };
                }
            });
        }
        else if (!lodash_1.default.isEqual(this.state.date, prevState.date)) {
            this.setState(function () {
                return {
                    currentMonth: this.state.months[this.state.date]
                };
            });
        }
        else if (!lodash_1.default.isEqual(this.state.name, prevState.name)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.hotel, prevState.hotel)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.departureDate, prevState.departureDate)) {
            this._validateFields();
        }
        else if (!lodash_1.default.isEqual(this.state.arrivalDate, prevState.arrivalDate)) {
            this._validateFields();
        }
    }
    render() {
        const dates = lodash_1.default.range(1, parseInt(this.state.currentMonth.days, 10) + 1);
        const getMonth = this.state.currentMonth.month;
        return (<Bookings_1.default getMonth={getMonth} _formatDates={this._formatDates(0, dates)} selectedDates={this.state.selectedDates} _selectDate={this._selectDate} _prevMonth={this._prevMonth} _nextMonth={this._nextMonth} arrivalDate={this.state.arrivalDate} departureDate={this.state.departureDate} hotel={this.state.hotel} _hotelSelection={this._hotelSelection} _nameInput={this._nameInput} name={this.state.name} _validateFields={this._validateFields} textColor={this.state.textColor} bgColor={this.state.bgColor}/>);
    }
}
/*
  Navigation Config
*/
BookingsContainer.navigationOptions = {
    title: 'Bookings',
    headerStyle: {
        backgroundColor: 'white'
    },
    headerTitleStyle: {
        color: 'gray',
        fontSize: 20
    },
};
exports.default = BookingsContainer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL1VzZXJzL3NoZXJyb2RqZW5raW5zL2NoYWxsZW5nZS9ob3RlbHJlc2VydmF0aW9ucy9jb21wb25lbnRzL2NvbnRhaW5lci9Cb29raW5nc0NvbnRhaW5lci50c3giLCJtYXBwaW5ncyI6IjtBQUFBOzs7Ozs7Ozs7R0FTRzs7Ozs7Ozs7Ozs7O0FBRUgsK0NBQXlDO0FBQ3pDLGlFQUF3QztBQUN4QyxvREFBdUI7QUF1QnZCLE1BQU0saUJBQWtCLFNBQVEsaUJBQWdCO0lBQWhEO1FBQ0U7O1VBRUU7O1FBRUYsVUFBSyxHQUFHO1lBQ04saUJBQWlCLEVBQUUsT0FBTztZQUMxQixLQUFLLEVBQUUsRUFBRTtZQUNULEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtZQUNmLGFBQWEsRUFBRSxFQUFFO1lBQ2pCLElBQUksRUFBRSxJQUFJLElBQUksRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMzQixRQUFRLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQztZQUM5QyxHQUFHLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFDekIsWUFBWSxFQUFFLEVBQWtCO1lBQ2hDLGdCQUFnQixFQUFFLEVBQUU7WUFDcEIsU0FBUyxFQUFFLENBQUM7WUFDWixrQkFBa0IsRUFBRSxDQUFDO1lBQ3JCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE1BQU0sRUFBRTtnQkFDTixFQUFDLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDakMsRUFBQyxPQUFPLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2xDLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUMvQixFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDL0IsRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQzdCLEVBQUMsT0FBTyxFQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUM5QixFQUFDLE9BQU8sRUFBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDOUIsRUFBQyxPQUFPLEVBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2hDLEVBQUMsT0FBTyxFQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2dCQUNuQyxFQUFDLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBQztnQkFDakMsRUFBQyxPQUFPLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUM7Z0JBQ2xDLEVBQUMsT0FBTyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDO2FBQ25DO1lBQ0QsT0FBTyxFQUFFLFNBQVM7WUFDbEIsU0FBUyxFQUFFLE1BQU07U0FDbEIsQ0FBQTtRQTJERDs7VUFFRTtRQUNGLHVCQUFrQixHQUFHLEdBQUcsRUFBRTtZQUN4QixJQUFJLGNBQWMsR0FBRyxnQkFBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxDQUFDLFVBQVMsQ0FBQyxJQUFHLE9BQU8sQ0FBQyxDQUFDLFlBQVksQ0FBQSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDOUYsSUFBSSxnQkFBZ0IsR0FBRyxnQkFBQyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFTLENBQUMsSUFBRyxPQUFPLENBQUMsQ0FBQyxhQUFhLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRXZGLE9BQU8sZ0JBQWdCLENBQUE7UUFDekIsQ0FBQyxDQUFBO1FBRUQsZ0JBQVcsR0FBRyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQzdCLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQTtZQUM3RCxNQUFNLFlBQVksR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxJQUFJLFFBQVEsQ0FBQTtZQUNyRSxNQUFNLGFBQWEsR0FBVyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUMvRCxNQUFNLFlBQVksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtZQUNyRCxNQUFNLFVBQVUsR0FBRztnQkFDakIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFVBQVUsRUFBRSxrQkFBa0I7Z0JBQzlCLGFBQWEsRUFBRSxhQUFhO2dCQUM1QixZQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSztnQkFDM0MsWUFBWSxFQUFFLFlBQVk7YUFDM0IsQ0FBQTtZQUNELE1BQU0sS0FBSyxHQUFrQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDeEUsTUFBTSxhQUFhLEdBQUcsZ0JBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsRUFBQyxjQUFjLEVBQUUsWUFBWSxFQUFPLENBQUMsQ0FBQTtZQUM1RixJQUFHLGdCQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxFQUFDO2dCQUNyRCxNQUFNLFdBQVcsR0FBRyxnQkFBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQTtnQkFDbkUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7Z0JBQ3pCLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO3dCQUNyQyxPQUFNOzRCQUNKLGFBQWEsRUFBRSxXQUFXOzRCQUMxQixhQUFhLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTs0QkFDeEQsV0FBVyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxZQUFZO3lCQUMxRixDQUFBO3FCQUNGO3lCQUFNLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQzt3QkFDNUMsT0FBTTs0QkFDSixhQUFhLEVBQUUsRUFBRTs0QkFDakIsV0FBVyxFQUFFLEVBQUU7eUJBQ2hCLENBQUE7cUJBQ0Y7eUJBQU07d0JBQ0wsT0FBTTs0QkFDSixhQUFhLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWTs0QkFDeEQsV0FBVyxFQUFFLEVBQUU7eUJBQ2hCLENBQUE7cUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUNaLE9BQU07d0JBQ0osYUFBYSxFQUFFLEtBQUs7cUJBQ3JCLENBQUE7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtRQUNILENBQUMsQ0FBQTtRQUVELGVBQVUsR0FBRyxHQUFHLEVBQUU7WUFDaEIsSUFBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUM7Z0JBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ1osT0FBTTt3QkFDSixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQztxQkFDMUIsQ0FBQTtnQkFDSCxDQUFDLENBQUMsQ0FBQTthQUNIO1FBQ0gsQ0FBQyxDQUFBO1FBRUQsZUFBVSxHQUFHLEdBQUcsRUFBRTtZQUNoQixJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBQztnQkFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQztvQkFDWixPQUFNO3dCQUNKLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDO3FCQUMxQixDQUFBO2dCQUNILENBQUMsQ0FBQyxDQUFBO2FBQ0g7UUFDSCxDQUFDLENBQUE7UUFFRCx5QkFBb0IsR0FBRyxHQUFHLEVBQUU7WUFDMUIsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFBO1lBQ2xELE1BQU0sUUFBUSxHQUFPLElBQUksSUFBSSxDQUFDLEdBQUcsWUFBWSxVQUFVLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFBO1lBRWxGLE9BQU8sUUFBUSxDQUFBO1FBQ2pCLENBQUMsQ0FBQTtRQUVELGlCQUFZLEdBQUcsQ0FBQyxRQUFnQixDQUFDLEVBQUUsS0FBb0IsRUFBRSxFQUFFO1lBQ3pELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQTtZQUNoQixJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUE7WUFFakIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUMsQ0FBQyxFQUFFLEVBQUM7Z0JBQzdCLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBO2FBQ3RFO1lBRUQsUUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUM7Z0JBQ25DLEtBQUssU0FBUztvQkFDWixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxVQUFVO29CQUNiLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLE9BQU87b0JBQ1YsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssT0FBTztvQkFDVixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxLQUFLO29CQUNSLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLE1BQU07b0JBQ1QsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssTUFBTTtvQkFDVCxTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxRQUFRO29CQUNYLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLFdBQVc7b0JBQ2QsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2dCQUNQLEtBQUssU0FBUztvQkFDWixTQUFTLEdBQUcsQ0FBQyxDQUFBO29CQUNiLE1BQUs7Z0JBQ1AsS0FBSyxVQUFVO29CQUNiLFNBQVMsR0FBRyxDQUFDLENBQUE7b0JBQ2IsTUFBSztnQkFDUCxLQUFLLFVBQVU7b0JBQ2IsU0FBUyxHQUFHLENBQUMsQ0FBQTtvQkFDYixNQUFLO2FBQ1I7WUFFRCxPQUFNLFNBQVMsS0FBSyxDQUFDLEVBQUM7Z0JBQ3BCLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO2dCQUNsQyxTQUFTLEVBQUUsQ0FBQTthQUNaO1lBRUQsUUFBTyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBQztnQkFDakMsS0FBSyxLQUFLO29CQUNSLEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO29CQUNqQixPQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBQzt3QkFDakIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO3dCQUVuRCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtxQkFDbEI7b0JBQ0QsTUFBSztnQkFDUCxLQUFLLEtBQUs7b0JBQ1IsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2pCLE9BQU0sS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFDO3dCQUNqQixLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUMsR0FBRyxFQUFFLFNBQVMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBRW5ELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO3FCQUNsQjtvQkFDRCxNQUFLO2dCQUNQLEtBQUssS0FBSztvQkFDUixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtvQkFDakIsT0FBTSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUM7d0JBQ2pCLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUUsU0FBUyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFFbkQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7cUJBQ2xCO29CQUNELE1BQUs7Z0JBQ1AsS0FBSyxLQUFLO29CQUNSLEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO29CQUNqQixPQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBQzt3QkFDakIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFDLEdBQUcsRUFBRSxTQUFTLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO3dCQUVuRCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtxQkFDbEI7b0JBQ0QsTUFBSztnQkFDUCxLQUFLLEtBQUs7b0JBQ1IsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2pCLE9BQU0sS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFDO3dCQUNqQixLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUMsR0FBRyxFQUFFLFNBQVMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBRW5ELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFBO3FCQUNsQjtvQkFDRCxNQUFLO2dCQUNQLEtBQUssS0FBSztvQkFDUixLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQTtvQkFDakIsT0FBTSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUM7d0JBQ2pCLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUUsU0FBUyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFFbkQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUE7cUJBQ2xCO29CQUNELE1BQUs7Z0JBQ1A7b0JBQ0UsS0FBSyxHQUFJLEtBQUssR0FBRyxDQUFDLENBQUE7b0JBQ2xCLE1BQUs7YUFDUjtZQUVELE9BQU8sS0FBSyxDQUFBO1FBQ2QsQ0FBQyxDQUFBO1FBRUQsb0JBQWUsR0FBRyxDQUFDLFNBQWlCLEVBQUUsRUFBRTtZQUN0QyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNaLE9BQU07b0JBQ0osS0FBSyxFQUFFLFNBQVM7aUJBQ2pCLENBQUE7WUFDSCxDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQTtRQUVELGVBQVUsR0FBRyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFBO1FBQ3ZCLENBQUMsQ0FBQTtRQUVELG9CQUFlLEdBQUcsQ0FBQyxJQUFJLEdBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsYUFBYSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFdBQVcsR0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxTQUFTLEdBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNqSixJQUFJLG9CQUFvQixHQUFvQixLQUFLLENBQUM7WUFFbEQsSUFBRyxJQUFJLEtBQUssRUFBRSxJQUFJLGFBQWEsS0FBSyxFQUFFLElBQUksV0FBVyxLQUFLLEVBQUUsSUFBSSxTQUFTLEtBQUssRUFBRSxFQUFDO2dCQUMvRSxJQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO29CQUNqQixJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLEVBQUM7d0JBQzNDLE9BQU0sQ0FDSixJQUFJLENBQUMsUUFBUSxDQUFDOzRCQUNaLE9BQU07Z0NBQ0osT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLE9BQU87NkJBQ25CLENBQUE7d0JBQ0gsQ0FBQyxDQUFDLENBQ0gsQ0FBQTtxQkFDRjtpQkFDRjthQUNGO1lBQ0QsT0FBTSxDQUNKLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ1osT0FBTTtvQkFDSixPQUFPLEVBQUUsU0FBUztvQkFDbEIsU0FBUyxFQUFFLE1BQU07aUJBQ2xCLENBQUE7WUFDSCxDQUFDLENBQUMsQ0FDSCxDQUFBO1FBRUgsQ0FBQyxDQUFBO0lBMEJILENBQUM7SUExU0M7O01BRUU7SUFDRixpQkFBaUI7UUFDZixJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ1osT0FBTTtnQkFDSixZQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7YUFDakQsQ0FBQTtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELGtCQUFrQixDQUFDLFNBQVMsRUFBRSxTQUFTO1FBQ3JDLElBQUcsQ0FBQyxnQkFBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUM7WUFDL0QsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7WUFDekIsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDWixJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7b0JBQ3JDLE9BQU07d0JBQ0osYUFBYSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7d0JBQ3hELFdBQVcsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsWUFBWTtxQkFDMUYsQ0FBQTtpQkFDRjtxQkFBTTtvQkFDTCxPQUFNO3dCQUNKLGFBQWEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZO3FCQUN6RCxDQUFBO2lCQUNGO1lBQ0gsQ0FBQyxDQUFDLENBQUE7U0FDSDthQUFNLElBQUcsQ0FBQyxnQkFBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUM7WUFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDWixPQUFNO29CQUNKLFlBQVksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztpQkFDakQsQ0FBQTtZQUNILENBQUMsQ0FBQyxDQUFBO1NBQ0g7YUFBTSxJQUFHLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFDO1lBQ2xELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN6QjthQUFNLElBQUcsQ0FBQyxnQkFBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUM7WUFDdEQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1NBQ3ZCO2FBQU0sSUFBRyxDQUFDLGdCQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsRUFBQztZQUN0RSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7U0FDdkI7YUFBTSxJQUFHLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFDO1lBQ2xFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN2QjtJQUNILENBQUM7SUF5T0QsTUFBTTtRQUNKLE1BQU0sS0FBSyxHQUFNLGdCQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBQzNFLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQTtRQUU5QyxPQUFNLENBQ0osQ0FBQyxrQkFBTyxDQUNOLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUNuQixZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUMxQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUN4QyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQzlCLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FDNUIsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUM1QixXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUNwQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUN4QyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUN4QixlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQ3RDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FDNUIsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FDdEIsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUN0QyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUNoQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUM1QixDQUNILENBQUE7SUFDSCxDQUFDOztBQXZURDs7RUFFRTtBQUNLLG1DQUFpQixHQUFHO0lBQ3pCLEtBQUssRUFBRSxVQUFVO0lBQ2pCLFdBQVcsRUFBRTtRQUNYLGVBQWUsRUFBRSxPQUFPO0tBQ3pCO0lBQ0QsZ0JBQWdCLEVBQUU7UUFDaEIsS0FBSyxFQUFFLE1BQU07UUFDYixRQUFRLEVBQUUsRUFBRTtLQUNiO0NBQ0YsQ0FBQztBQThTSixrQkFBZSxpQkFBaUIsQ0FBQyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvVXNlcnMvc2hlcnJvZGplbmtpbnMvY2hhbGxlbmdlL2hvdGVscmVzZXJ2YXRpb25zL2NvbXBvbmVudHMvY29udGFpbmVyL0Jvb2tpbmdzQ29udGFpbmVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFNhbXBsZSBSZWFjdCBOYXRpdmUgQXBwXG4gKiBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svcmVhY3QtbmF0aXZlXG4gKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqIEBsaW50LWlnbm9yZS1ldmVyeSBYUExBVEpTQ09QWVJJR0hUMVxuICogXG4gKiBCeSBTaGVycm9kIEplbmtpbnNcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IEJvb2tpbmcgZnJvbSAnLi4vY2xhc3MvQm9va2luZ3MnO1xuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcblxuaW50ZXJmYWNlIFN0YXRlIHtcbiAgaW5hY3RpdmVEYXRlQ29sb3I/OiBzdHJpbmcsXG4gIGRhdGVzPzogb2JqZWN0LFxuICBob3RlbD86IHN0cmluZywgXG4gIG5hbWU/OiBzdHJpbmcsXG4gIGFycml2YWxEYXRlPzogc3RyaW5nLFxuICBkZXBhcnR1cmVEYXRlPzogc3RyaW5nLFxuICBkYXRlPzogbnVtYmVyLFxuICBmdWxsRGF0ZT86IHN0cmluZyxcbiAgZGF5PzogbnVtYmVyLFxuICB0b3VjaGVkPzogQm9vbGVhbixcbiAgbnVtYmVyT2ZSb3dzPzogbnVtYmVyLFxuICBkYXRlQ2hhbmdlPzogQm9vbGVhbixcbiAgY3VycmVudE1vbnRoPzogc3RyaW5nW11cbn1cblxuaW50ZXJmYWNlIGN1cnJlbnRNb250aCB7XG4gIG1vbnRoOiBzdHJpbmcsXG4gIGRheXM6IHN0cmluZ1xufVxuXG5jbGFzcyBCb29raW5nc0NvbnRhaW5lciBleHRlbmRzIENvbXBvbmVudDxTdGF0ZT4ge1xuICAvKlxuICAgIEluaXRpYWwgU3RhdGVcbiAgKi9cblxuICBzdGF0ZSA9IHtcbiAgICBpbmFjdGl2ZURhdGVDb2xvcjogJ3doaXRlJyxcbiAgICBkYXRlczogW10sXG4gICAgaG90ZWw6ICcnLCBcbiAgICBuYW1lOiAnJyxcbiAgICBzZWxlY3RlZERhdGVzOiBbXSxcbiAgICBhcnJpdmFsRGF0ZTogXCJcIixcbiAgICBkZXBhcnR1cmVEYXRlOiBcIlwiLFxuICAgIGRhdGU6IG5ldyBEYXRlKCkuZ2V0TW9udGgoKSxcbiAgICBmdWxsRGF0ZTogbmV3IERhdGUoKS50b1N0cmluZygpLnN1YnN0cmluZygwLDMpLFxuICAgIGRheTogbmV3IERhdGUoKS5nZXREYXRlKCksXG4gICAgY3VycmVudE1vbnRoOiB7fSBhcyBjdXJyZW50TW9udGgsXG4gICAgZm9ybWF0RGF0ZXNBcnJheTogW10sXG4gICAgbnVtT2ZSb3dzOiA3LFxuICAgIG51bWJlck9mRXhlY3V0aW9uczogMCxcbiAgICByZW1vdmVEYXRlOiBmYWxzZSxcbiAgICBkYXRlQ2hhbmdlOiBmYWxzZSxcbiAgICBtb250aHM6IFtcbiAgICAgIHtcIm1vbnRoXCI6XCJKYW51YXJ5XCIsIFwiZGF5c1wiOiBcIjMxXCJ9LFxuICAgICAge1wibW9udGhcIjpcIkZlYnJ1YXJ5XCIsIFwiZGF5c1wiOiBcIjI4XCJ9LFxuICAgICAge1wibW9udGhcIjpcIk1hcmNoXCIsIFwiZGF5c1wiOiBcIjMxXCJ9LFxuICAgICAge1wibW9udGhcIjpcIkFwcmlsXCIsIFwiZGF5c1wiOiBcIjMwXCJ9LFxuICAgICAge1wibW9udGhcIjpcIk1heVwiLCBcImRheXNcIjogXCIzMVwifSwgXG4gICAgICB7XCJtb250aFwiOlwiSnVuZVwiLCBcImRheXNcIjogXCIzMFwifSwgXG4gICAgICB7XCJtb250aFwiOlwiSnVseVwiLCBcImRheXNcIjogXCIzMVwifSxcbiAgICAgIHtcIm1vbnRoXCI6XCJBdWd1c3RcIiwgXCJkYXlzXCI6IFwiMzFcIn0sXG4gICAgICB7XCJtb250aFwiOlwiU2VwdGVtYmVyXCIsIFwiZGF5c1wiOiBcIjMwXCJ9LFxuICAgICAge1wibW9udGhcIjpcIk9jdG9iZXJcIiwgXCJkYXlzXCI6IFwiMzFcIn0sXG4gICAgICB7XCJtb250aFwiOlwiTm92ZW1iZXJcIiwgXCJkYXlzXCI6IFwiMzBcIn0sXG4gICAgICB7XCJtb250aFwiOlwiRGVjZW1iZXJcIiwgXCJkYXlzXCI6IFwiMzFcIn1cbiAgICBdLCBcbiAgICBiZ0NvbG9yOiAnIzBiMWMzYScsXG4gICAgdGV4dENvbG9yOiAnZ3JheSdcbiAgfVxuXG4gIC8qXG4gICAgTmF2aWdhdGlvbiBDb25maWdcbiAgKi8gIFxuICBzdGF0aWMgbmF2aWdhdGlvbk9wdGlvbnMgPSB7XG4gICAgdGl0bGU6ICdCb29raW5ncycsXG4gICAgaGVhZGVyU3R5bGU6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ3doaXRlJ1xuICAgIH0sXG4gICAgaGVhZGVyVGl0bGVTdHlsZToge1xuICAgICAgY29sb3I6ICdncmF5JyxcbiAgICAgIGZvbnRTaXplOiAyMFxuICAgIH0sXG4gIH07XG5cbiAgLypcbiAgICBMaWZlY3ljbGUgTWV0aG9kc1xuICAqL1xuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICByZXR1cm57XG4gICAgICAgIGN1cnJlbnRNb250aDogdGhpcy5zdGF0ZS5tb250aHNbdGhpcy5zdGF0ZS5kYXRlXVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzLCBwcmV2U3RhdGUpe1xuICAgIGlmKCFfLmlzRXF1YWwodGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLCBwcmV2U3RhdGUuc2VsZWN0ZWREYXRlcykpe1xuICAgICAgdGhpcy5fc29ydFNlbGVjdGVkRGF0ZXMoKVxuICAgICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgICBpZih0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMubGVuZ3RoID4gMSl7XG4gICAgICAgICAgcmV0dXJueyBcbiAgICAgICAgICAgIGRlcGFydHVyZURhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbMF0uZGF0ZUFzU3RyaW5nLFxuICAgICAgICAgICAgYXJyaXZhbERhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbdGhpcy5fc29ydFNlbGVjdGVkRGF0ZXMoKS5sZW5ndGggLSAxXS5kYXRlQXNTdHJpbmdcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJue1xuICAgICAgICAgICAgZGVwYXJ0dXJlRGF0ZTogdGhpcy5fc29ydFNlbGVjdGVkRGF0ZXMoKVswXS5kYXRlQXNTdHJpbmcsXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH0gZWxzZSBpZighXy5pc0VxdWFsKHRoaXMuc3RhdGUuZGF0ZSwgcHJldlN0YXRlLmRhdGUpKXtcbiAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJue1xuICAgICAgICAgIGN1cnJlbnRNb250aDogdGhpcy5zdGF0ZS5tb250aHNbdGhpcy5zdGF0ZS5kYXRlXVxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH0gZWxzZSBpZighXy5pc0VxdWFsKHRoaXMuc3RhdGUubmFtZSwgcHJldlN0YXRlLm5hbWUpKXtcbiAgICAgICAgdGhpcy5fdmFsaWRhdGVGaWVsZHMoKVxuICAgIH0gZWxzZSBpZighXy5pc0VxdWFsKHRoaXMuc3RhdGUuaG90ZWwsIHByZXZTdGF0ZS5ob3RlbCkpe1xuICAgICAgdGhpcy5fdmFsaWRhdGVGaWVsZHMoKVxuICAgIH0gZWxzZSBpZighXy5pc0VxdWFsKHRoaXMuc3RhdGUuZGVwYXJ0dXJlRGF0ZSwgcHJldlN0YXRlLmRlcGFydHVyZURhdGUpKXtcbiAgICAgIHRoaXMuX3ZhbGlkYXRlRmllbGRzKClcbiAgICB9IGVsc2UgaWYoIV8uaXNFcXVhbCh0aGlzLnN0YXRlLmFycml2YWxEYXRlLCBwcmV2U3RhdGUuYXJyaXZhbERhdGUpKXtcbiAgICAgIHRoaXMuX3ZhbGlkYXRlRmllbGRzKClcbiAgICB9ICAgICBcbiAgfVxuXG4gIC8qXG4gICAgRnVuY3Rpb25zXG4gICovXG4gIF9zb3J0U2VsZWN0ZWREYXRlcyA9ICgpID0+IHtcbiAgICBsZXQgc29ydGVkRGF5QXJyYXkgPSBfLnNvcnRCeSh0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMsIFtmdW5jdGlvbihhKXsgcmV0dXJuIGEuZGF0ZUFzTnVtYmVyfV0pXG4gICAgbGV0IHNvcnRlZE1vbnRoQXJyYXkgPSBfLnNvcnRCeShzb3J0ZWREYXlBcnJheSwgW2Z1bmN0aW9uKGEpeyByZXR1cm4gYS5tb250aEFzTnVtYmVyfV0pXG5cbiAgICByZXR1cm4gc29ydGVkTW9udGhBcnJheVxuICB9XG5cbiAgX3NlbGVjdERhdGUgPSAoZGF0ZTogbnVtYmVyKSA9PiB7XG4gICAgbGV0IGN1cnJlbnRNb250aExlbmd0aCA9IHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoLmxlbmd0aCBcbiAgICBjb25zdCBkYXRlQXNTdHJpbmcgPSBgJHt0aGlzLnN0YXRlLmN1cnJlbnRNb250aC5tb250aH0gJHtkYXRlfSwgMjAxOWBcbiAgICBjb25zdCBtb250aEFzTnVtYmVyOiBudW1iZXIgPSBuZXcgRGF0ZShkYXRlQXNTdHJpbmcpLmdldE1vbnRoKClcbiAgICBjb25zdCBkYXRlQXNOdW1iZXIgPSBuZXcgRGF0ZShkYXRlQXNTdHJpbmcpLmdldERhdGUoKVxuICAgIGNvbnN0IGRhdGVPYmplY3QgPSB7XG4gICAgICBkYXRlQXNTdHJpbmc6IGRhdGVBc1N0cmluZywgXG4gICAgICBkYXRlTGVuZ3RoOiBjdXJyZW50TW9udGhMZW5ndGgsIFxuICAgICAgbW9udGhBc051bWJlcjogbW9udGhBc051bWJlciwgXG4gICAgICBjdXJyZW50TW9udGg6IHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoLFxuICAgICAgZGF0ZUFzTnVtYmVyOiBkYXRlQXNOdW1iZXJcbiAgICB9XG4gICAgY29uc3QgZGF0ZXM6IEFycmF5PG51bWJlcj4gPSB0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMuY29uY2F0KGRhdGVPYmplY3QpXG4gICAgY29uc3QgZHVwbGljYXRlRGF0ZSA9IF8uZmluZCh0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMsIHtcImRhdGVBc1N0cmluZ1wiOiBkYXRlQXNTdHJpbmd9IGFzIHt9KVxuICAgIGlmKF8uaW5jbHVkZXModGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzLCBkdXBsaWNhdGVEYXRlKSl7XG4gICAgICBjb25zdCByZW1vdmVkRGF0ZSA9IF8ucHVsbCh0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMsIGR1cGxpY2F0ZURhdGUpXG4gICAgICB0aGlzLl9zb3J0U2VsZWN0ZWREYXRlcygpXG4gICAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgIGlmKHRoaXMuc3RhdGUuc2VsZWN0ZWREYXRlcy5sZW5ndGggPiAxKXtcbiAgICAgICAgICByZXR1cm57XG4gICAgICAgICAgICBzZWxlY3RlZERhdGVzOiByZW1vdmVkRGF0ZSxcbiAgICAgICAgICAgIGRlcGFydHVyZURhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbMF0uZGF0ZUFzU3RyaW5nLFxuICAgICAgICAgICAgYXJyaXZhbERhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbdGhpcy5fc29ydFNlbGVjdGVkRGF0ZXMoKS5sZW5ndGggLSAxXS5kYXRlQXNTdHJpbmdcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZih0aGlzLnN0YXRlLnNlbGVjdGVkRGF0ZXMubGVuZ3RoIDwgMSl7XG4gICAgICAgICAgcmV0dXJue1xuICAgICAgICAgICAgZGVwYXJ0dXJlRGF0ZTogJycsXG4gICAgICAgICAgICBhcnJpdmFsRGF0ZTogJycsXG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybntcbiAgICAgICAgICAgIGRlcGFydHVyZURhdGU6IHRoaXMuX3NvcnRTZWxlY3RlZERhdGVzKClbMF0uZGF0ZUFzU3RyaW5nLFxuICAgICAgICAgICAgYXJyaXZhbERhdGU6ICcnLFxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm57XG4gICAgICAgICAgc2VsZWN0ZWREYXRlczogZGF0ZXNcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBfcHJldk1vbnRoID0gKCkgPT4ge1xuICAgIGlmKHRoaXMuc3RhdGUuZGF0ZSA+PSAxKXtcbiAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJue1xuICAgICAgICAgIGRhdGU6IHRoaXMuc3RhdGUuZGF0ZSAtIDFcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBfbmV4dE1vbnRoID0gKCkgPT4ge1xuICAgIGlmKHRoaXMuc3RhdGUuZGF0ZSA8PSAxMCl7XG4gICAgICB0aGlzLnNldFN0YXRlKGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybntcbiAgICAgICAgICBkYXRlOiB0aGlzLnN0YXRlLmRhdGUgKyAxXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgX2ZpcnN0RGF5VGhlTW9udGhEYXkgPSAoKSA9PiB7XG4gICAgY29uc3QgY3VycmVudE1vbnRoID0gdGhpcy5zdGF0ZS5jdXJyZW50TW9udGgubW9udGggXG4gICAgY29uc3QgZmlyc3REYXkgICAgID0gbmV3IERhdGUoYCR7Y3VycmVudE1vbnRofSAxLCAyMDE5YCkudG9TdHJpbmcoKS5zdWJzdHJpbmcoMCwzKSBcblxuICAgIHJldHVybiBmaXJzdERheVxuICB9XG5cbiAgX2Zvcm1hdERhdGVzID0gKGNvdW50OiBudW1iZXIgPSAwLCBkYXRlczogQXJyYXk8bnVtYmVyPikgPT4ge1xuICAgIGNvbnN0IGFycmF5ID0gW11cbiAgICB2YXIgY291bnRMb29wID0gMCBcblxuICAgIGZvcih2YXIgeD0wO3g8ZGF0ZXMubGVuZ3RoO3grKyl7XG4gICAgICBhcnJheS5wdXNoKHtcIm1vbnRoXCI6IHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoLCBcImRheVwiOiBkYXRlc1t4XX0pXG4gICAgfVxuXG4gICAgc3dpdGNoKHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoKXtcbiAgICAgIGNhc2UgJ0phbnVhcnknIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDJcbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgJ0ZlYnJ1YXJ5JyA6IFxuICAgICAgICBjb3VudExvb3AgPSAyXG4gICAgICAgIGJyZWFrICBcbiAgICAgIGNhc2UgJ01hcmNoJyA6IFxuICAgICAgICBjb3VudExvb3AgPSA2XG4gICAgICAgIGJyZWFrICBcbiAgICAgIGNhc2UgJ0FwcmlsJyA6IFxuICAgICAgICBjb3VudExvb3AgPSA0XG4gICAgICAgIGJyZWFrICBcbiAgICAgIGNhc2UgJ01heScgOiBcbiAgICAgICAgY291bnRMb29wID0gMVxuICAgICAgICBicmVhayBcbiAgICAgIGNhc2UgJ0p1bmUnIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDZcbiAgICAgICAgYnJlYWsgICBcbiAgICAgIGNhc2UgJ0p1bHknIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDNcbiAgICAgICAgYnJlYWsgIFxuICAgICAgY2FzZSAnQXVndXN0JyA6IFxuICAgICAgICBjb3VudExvb3AgPSAwXG4gICAgICAgIGJyZWFrXG4gICAgICBjYXNlICdTZXB0ZW1iZXInIDogXG4gICAgICAgIGNvdW50TG9vcCA9IDVcbiAgICAgICAgYnJlYWsgXG4gICAgICBjYXNlICdPY3RvYmVyJyA6IFxuICAgICAgICBjb3VudExvb3AgPSAyXG4gICAgICAgIGJyZWFrICAgXG4gICAgICBjYXNlICdOb3ZlbWJlcicgOiBcbiAgICAgICAgY291bnRMb29wID0gMFxuICAgICAgICBicmVhayAgXG4gICAgICBjYXNlICdEZWNlbWJlcicgOiBcbiAgICAgICAgY291bnRMb29wID0gNFxuICAgICAgICBicmVhayAgXG4gICAgfVxuXG4gICAgd2hpbGUoY291bnRMb29wICE9PSAwKXsgXG4gICAgICBhcnJheS5wdXNoKHtrZXk6IFwiXCIsIGVtcHR5OiB0cnVlfSlcbiAgICAgIGNvdW50TG9vcC0tXG4gICAgfVxuXG4gICAgc3dpdGNoKHRoaXMuX2ZpcnN0RGF5VGhlTW9udGhEYXkoKSl7XG4gICAgICBjYXNlICdUdWUnIDpcbiAgICAgICAgY291bnQgPSBjb3VudCArIDFcbiAgICAgICAgd2hpbGUoY291bnQgIT09IC0xKXtcbiAgICAgICAgICBhcnJheS51bnNoaWZ0KHtrZXk6IGBibGFuay0ke2NvdW50fWAsIGVtcHR5OiB0cnVlfSkgXG5cbiAgICAgICAgICBjb3VudCA9IGNvdW50IC0gMVxuICAgICAgICB9XG4gICAgICAgIGJyZWFrIFxuICAgICAgY2FzZSAnV2VkJyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyAyXG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnVGh1JyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyAzXG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnRnJpJyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyA0XG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnU2F0JyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyA1XG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVha1xuICAgICAgY2FzZSAnTW9uJyA6XG4gICAgICAgIGNvdW50ID0gY291bnQgKyAwXG4gICAgICAgIHdoaWxlKGNvdW50ICE9PSAtMSl7XG4gICAgICAgICAgYXJyYXkudW5zaGlmdCh7a2V5OiBgYmxhbmstJHtjb3VudH1gLCBlbXB0eTogdHJ1ZX0pIFxuXG4gICAgICAgICAgY291bnQgPSBjb3VudCAtIDFcbiAgICAgICAgfVxuICAgICAgICBicmVha1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgY291bnQgPSAgY291bnQgKyAwICBcbiAgICAgICAgYnJlYWtcbiAgICB9IFxuICAgIFxuICAgIHJldHVybiBhcnJheVxuICB9XG5cbiAgX2hvdGVsU2VsZWN0aW9uID0gKGl0ZW1WYWx1ZTogc3RyaW5nKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgcmV0dXJue1xuICAgICAgICBob3RlbDogaXRlbVZhbHVlXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIF9uYW1lSW5wdXQgPSAobmFtZTogc3RyaW5nKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7bmFtZX0pXG4gIH1cblxuICBfdmFsaWRhdGVGaWVsZHMgPSAobmFtZT10aGlzLnN0YXRlLm5hbWUsIGRlcGFydHVyZURhdGU9dGhpcy5zdGF0ZS5kZXBhcnR1cmVEYXRlLCBhcnJpdmFsRGF0ZT10aGlzLnN0YXRlLmFycml2YWxEYXRlLCBob3RlbE5hbWU9dGhpcy5zdGF0ZS5ob3RlbCkgPT4ge1xuICAgIGxldCB3aGl0ZXNwYWNlQ2hhclNlYXJjaCAgICAgICAgICAgICAgICAgID0gL1xccy9nO1xuICAgIFxuICAgIGlmKG5hbWUgIT09IFwiXCIgJiYgZGVwYXJ0dXJlRGF0ZSAhPT0gXCJcIiAmJiBhcnJpdmFsRGF0ZSAhPT0gXCJcIiAmJiBob3RlbE5hbWUgIT09IFwiXCIpe1xuICAgICAgaWYobmFtZS5sZW5ndGggPiA0KXtcbiAgICAgICAgaWYobmFtZS5tYXRjaCh3aGl0ZXNwYWNlQ2hhclNlYXJjaCkgIT09IG51bGwpe1xuICAgICAgICAgIHJldHVybihcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgcmV0dXJue1xuICAgICAgICAgICAgICAgIGJnQ29sb3I6IFwiYmx1ZVwiLFxuICAgICAgICAgICAgICAgIHRleHRDb2xvcjogXCJ3aGl0ZVwiIFxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICkgIFxuICAgICAgICB9IFxuICAgICAgfSBcbiAgICB9XG4gICAgcmV0dXJuKFxuICAgICAgdGhpcy5zZXRTdGF0ZShmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm57XG4gICAgICAgICAgYmdDb2xvcjogXCIjMGIxYzNhXCIsXG4gICAgICAgICAgdGV4dENvbG9yOiBcImdyYXlcIiBcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApICAgIFxuICAgICAgXG4gIH1cblxuICByZW5kZXIoKXtcbiAgICBjb25zdCBkYXRlcyAgICA9IF8ucmFuZ2UoMSwgcGFyc2VJbnQodGhpcy5zdGF0ZS5jdXJyZW50TW9udGguZGF5cywgMTApICsgMSlcbiAgICBjb25zdCBnZXRNb250aCA9IHRoaXMuc3RhdGUuY3VycmVudE1vbnRoLm1vbnRoIFxuXG4gICAgcmV0dXJuKFxuICAgICAgPEJvb2tpbmdcbiAgICAgICAgZ2V0TW9udGg9e2dldE1vbnRofVxuICAgICAgICBfZm9ybWF0RGF0ZXM9e3RoaXMuX2Zvcm1hdERhdGVzKDAsIGRhdGVzKX1cbiAgICAgICAgc2VsZWN0ZWREYXRlcz17dGhpcy5zdGF0ZS5zZWxlY3RlZERhdGVzfVxuICAgICAgICBfc2VsZWN0RGF0ZT17dGhpcy5fc2VsZWN0RGF0ZX1cbiAgICAgICAgX3ByZXZNb250aD17dGhpcy5fcHJldk1vbnRofVxuICAgICAgICBfbmV4dE1vbnRoPXt0aGlzLl9uZXh0TW9udGh9XG4gICAgICAgIGFycml2YWxEYXRlPXt0aGlzLnN0YXRlLmFycml2YWxEYXRlfVxuICAgICAgICBkZXBhcnR1cmVEYXRlPXt0aGlzLnN0YXRlLmRlcGFydHVyZURhdGV9XG4gICAgICAgIGhvdGVsPXt0aGlzLnN0YXRlLmhvdGVsfVxuICAgICAgICBfaG90ZWxTZWxlY3Rpb249e3RoaXMuX2hvdGVsU2VsZWN0aW9ufVxuICAgICAgICBfbmFtZUlucHV0PXt0aGlzLl9uYW1lSW5wdXR9XG4gICAgICAgIG5hbWU9e3RoaXMuc3RhdGUubmFtZX1cbiAgICAgICAgX3ZhbGlkYXRlRmllbGRzPXt0aGlzLl92YWxpZGF0ZUZpZWxkc31cbiAgICAgICAgdGV4dENvbG9yPXt0aGlzLnN0YXRlLnRleHRDb2xvcn1cbiAgICAgICAgYmdDb2xvcj17dGhpcy5zdGF0ZS5iZ0NvbG9yfVxuICAgICAgLz5cbiAgICApXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQm9va2luZ3NDb250YWluZXI7XG4iXSwidmVyc2lvbiI6M30=