7c10fac160f0e5909cea4ae8ea2ada57
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var AnimatedValue = require('./nodes/AnimatedValue');

var NativeAnimatedHelper = require('./NativeAnimatedHelper');

var ReactNative = require('../../Renderer/shims/ReactNative');

var invariant = require('fbjs/lib/invariant');

var _require = require('./NativeAnimatedHelper'),
    shouldUseNativeDriver = _require.shouldUseNativeDriver;

function attachNativeEvent(viewRef, eventName, argMapping) {
  var eventMappings = [];

  var traverse = function traverse(value, path) {
    if (value instanceof AnimatedValue) {
      value.__makeNative();

      eventMappings.push({
        nativeEventPath: path,
        animatedValueTag: value.__getNativeTag()
      });
    } else if (typeof value === 'object') {
      for (var _key in value) {
        traverse(value[_key], path.concat(_key));
      }
    }
  };

  invariant(argMapping[0] && argMapping[0].nativeEvent, 'Native driven events only support animated values contained inside `nativeEvent`.');
  traverse(argMapping[0].nativeEvent, []);
  var viewTag = ReactNative.findNodeHandle(viewRef);
  eventMappings.forEach(function (mapping) {
    NativeAnimatedHelper.API.addAnimatedEventToView(viewTag, eventName, mapping);
  });
  return {
    detach: function detach() {
      eventMappings.forEach(function (mapping) {
        NativeAnimatedHelper.API.removeAnimatedEventFromView(viewTag, eventName, mapping.animatedValueTag);
      });
    }
  };
}

var AnimatedEvent = function () {
  function AnimatedEvent(argMapping) {
    var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck2.default)(this, AnimatedEvent);
    this._listeners = [];
    this._argMapping = argMapping;

    if (config.listener) {
      this.__addListener(config.listener);
    }

    this._callListeners = this._callListeners.bind(this);
    this._attachedEvent = null;
    this.__isNative = shouldUseNativeDriver(config);

    if (__DEV__) {
      this._validateMapping();
    }
  }

  (0, _createClass2.default)(AnimatedEvent, [{
    key: "__addListener",
    value: function __addListener(callback) {
      this._listeners.push(callback);
    }
  }, {
    key: "__removeListener",
    value: function __removeListener(callback) {
      this._listeners = this._listeners.filter(function (listener) {
        return listener !== callback;
      });
    }
  }, {
    key: "__attach",
    value: function __attach(viewRef, eventName) {
      invariant(this.__isNative, 'Only native driven events need to be attached.');
      this._attachedEvent = attachNativeEvent(viewRef, eventName, this._argMapping);
    }
  }, {
    key: "__detach",
    value: function __detach(viewTag, eventName) {
      invariant(this.__isNative, 'Only native driven events need to be detached.');
      this._attachedEvent && this._attachedEvent.detach();
    }
  }, {
    key: "__getHandler",
    value: function __getHandler() {
      var _this = this;

      if (this.__isNative) {
        return this._callListeners;
      }

      return function () {
        for (var _len = arguments.length, args = new Array(_len), _key2 = 0; _key2 < _len; _key2++) {
          args[_key2] = arguments[_key2];
        }

        var traverse = function traverse(recMapping, recEvt, key) {
          if (typeof recEvt === 'number' && recMapping instanceof AnimatedValue) {
            recMapping.setValue(recEvt);
          } else if (typeof recMapping === 'object') {
            for (var mappingKey in recMapping) {
              traverse(recMapping[mappingKey], recEvt[mappingKey], mappingKey);
            }
          }
        };

        if (!_this.__isNative) {
          _this._argMapping.forEach(function (mapping, idx) {
            traverse(mapping, args[idx], 'arg' + idx);
          });
        }

        _this._callListeners.apply(_this, args);
      };
    }
  }, {
    key: "_callListeners",
    value: function _callListeners() {
      for (var _len2 = arguments.length, args = new Array(_len2), _key3 = 0; _key3 < _len2; _key3++) {
        args[_key3] = arguments[_key3];
      }

      this._listeners.forEach(function (listener) {
        return listener.apply(void 0, args);
      });
    }
  }, {
    key: "_validateMapping",
    value: function _validateMapping() {
      var traverse = function traverse(recMapping, recEvt, key) {
        if (typeof recEvt === 'number') {
          invariant(recMapping instanceof AnimatedValue, 'Bad mapping of type ' + typeof recMapping + ' for key ' + key + ', event value must map to AnimatedValue');
          return;
        }

        invariant(typeof recMapping === 'object', 'Bad mapping of type ' + typeof recMapping + ' for key ' + key);
        invariant(typeof recEvt === 'object', 'Bad event of type ' + typeof recEvt + ' for key ' + key);

        for (var mappingKey in recMapping) {
          traverse(recMapping[mappingKey], recEvt[mappingKey], mappingKey);
        }
      };
    }
  }]);
  return AnimatedEvent;
}();

module.exports = {
  AnimatedEvent: AnimatedEvent,
  attachNativeEvent: attachNativeEvent
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkFuaW1hdGVkRXZlbnQuanMiXSwibmFtZXMiOlsiQW5pbWF0ZWRWYWx1ZSIsInJlcXVpcmUiLCJOYXRpdmVBbmltYXRlZEhlbHBlciIsIlJlYWN0TmF0aXZlIiwiaW52YXJpYW50Iiwic2hvdWxkVXNlTmF0aXZlRHJpdmVyIiwiYXR0YWNoTmF0aXZlRXZlbnQiLCJ2aWV3UmVmIiwiZXZlbnROYW1lIiwiYXJnTWFwcGluZyIsImV2ZW50TWFwcGluZ3MiLCJ0cmF2ZXJzZSIsInZhbHVlIiwicGF0aCIsIl9fbWFrZU5hdGl2ZSIsInB1c2giLCJuYXRpdmVFdmVudFBhdGgiLCJhbmltYXRlZFZhbHVlVGFnIiwiX19nZXROYXRpdmVUYWciLCJrZXkiLCJjb25jYXQiLCJuYXRpdmVFdmVudCIsInZpZXdUYWciLCJmaW5kTm9kZUhhbmRsZSIsImZvckVhY2giLCJtYXBwaW5nIiwiQVBJIiwiYWRkQW5pbWF0ZWRFdmVudFRvVmlldyIsImRldGFjaCIsInJlbW92ZUFuaW1hdGVkRXZlbnRGcm9tVmlldyIsIkFuaW1hdGVkRXZlbnQiLCJjb25maWciLCJfbGlzdGVuZXJzIiwiX2FyZ01hcHBpbmciLCJsaXN0ZW5lciIsIl9fYWRkTGlzdGVuZXIiLCJfY2FsbExpc3RlbmVycyIsImJpbmQiLCJfYXR0YWNoZWRFdmVudCIsIl9faXNOYXRpdmUiLCJfX0RFVl9fIiwiX3ZhbGlkYXRlTWFwcGluZyIsImNhbGxiYWNrIiwiZmlsdGVyIiwiYXJncyIsInJlY01hcHBpbmciLCJyZWNFdnQiLCJzZXRWYWx1ZSIsIm1hcHBpbmdLZXkiLCJpZHgiLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiQUFTQTs7Ozs7Ozs7QUFFQSxJQUFNQSxhQUFhLEdBQUdDLE9BQU8sQ0FBQyx1QkFBRCxDQUE3Qjs7QUFDQSxJQUFNQyxvQkFBb0IsR0FBR0QsT0FBTyxDQUFDLHdCQUFELENBQXBDOztBQUNBLElBQU1FLFdBQVcsR0FBR0YsT0FBTyxDQUFDLGtDQUFELENBQTNCOztBQUVBLElBQU1HLFNBQVMsR0FBR0gsT0FBTyxDQUFDLG9CQUFELENBQXpCOztlQUNnQ0EsT0FBTyxDQUFDLHdCQUFELEM7SUFBaENJLHFCLFlBQUFBLHFCOztBQVFQLFNBQVNDLGlCQUFULENBQ0VDLE9BREYsRUFFRUMsU0FGRixFQUdFQyxVQUhGLEVBSUU7QUFHQSxNQUFNQyxhQUFhLEdBQUcsRUFBdEI7O0FBRUEsTUFBTUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0MsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQ2hDLFFBQUlELEtBQUssWUFBWVosYUFBckIsRUFBb0M7QUFDbENZLE1BQUFBLEtBQUssQ0FBQ0UsWUFBTjs7QUFFQUosTUFBQUEsYUFBYSxDQUFDSyxJQUFkLENBQW1CO0FBQ2pCQyxRQUFBQSxlQUFlLEVBQUVILElBREE7QUFFakJJLFFBQUFBLGdCQUFnQixFQUFFTCxLQUFLLENBQUNNLGNBQU47QUFGRCxPQUFuQjtBQUlELEtBUEQsTUFPTyxJQUFJLE9BQU9OLEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7QUFDcEMsV0FBSyxJQUFNTyxJQUFYLElBQWtCUCxLQUFsQixFQUF5QjtBQUN2QkQsUUFBQUEsUUFBUSxDQUFDQyxLQUFLLENBQUNPLElBQUQsQ0FBTixFQUFhTixJQUFJLENBQUNPLE1BQUwsQ0FBWUQsSUFBWixDQUFiLENBQVI7QUFDRDtBQUNGO0FBQ0YsR0FiRDs7QUFlQWYsRUFBQUEsU0FBUyxDQUNQSyxVQUFVLENBQUMsQ0FBRCxDQUFWLElBQWlCQSxVQUFVLENBQUMsQ0FBRCxDQUFWLENBQWNZLFdBRHhCLEVBRVAsbUZBRk8sQ0FBVDtBQU1BVixFQUFBQSxRQUFRLENBQUNGLFVBQVUsQ0FBQyxDQUFELENBQVYsQ0FBY1ksV0FBZixFQUE0QixFQUE1QixDQUFSO0FBRUEsTUFBTUMsT0FBTyxHQUFHbkIsV0FBVyxDQUFDb0IsY0FBWixDQUEyQmhCLE9BQTNCLENBQWhCO0FBRUFHLEVBQUFBLGFBQWEsQ0FBQ2MsT0FBZCxDQUFzQixVQUFBQyxPQUFPLEVBQUk7QUFDL0J2QixJQUFBQSxvQkFBb0IsQ0FBQ3dCLEdBQXJCLENBQXlCQyxzQkFBekIsQ0FDRUwsT0FERixFQUVFZCxTQUZGLEVBR0VpQixPQUhGO0FBS0QsR0FORDtBQVFBLFNBQU87QUFDTEcsSUFBQUEsTUFESyxvQkFDSTtBQUNQbEIsTUFBQUEsYUFBYSxDQUFDYyxPQUFkLENBQXNCLFVBQUFDLE9BQU8sRUFBSTtBQUMvQnZCLFFBQUFBLG9CQUFvQixDQUFDd0IsR0FBckIsQ0FBeUJHLDJCQUF6QixDQUNFUCxPQURGLEVBRUVkLFNBRkYsRUFHRWlCLE9BQU8sQ0FBQ1IsZ0JBSFY7QUFLRCxPQU5EO0FBT0Q7QUFUSSxHQUFQO0FBV0Q7O0lBRUthLGE7QUFTSix5QkFBWXJCLFVBQVosRUFBb0U7QUFBQSxRQUEzQnNCLE1BQTJCLHVFQUFKLEVBQUk7QUFBQTtBQUFBLFNBUHBFQyxVQU9vRSxHQVB0QyxFQU9zQztBQUNsRSxTQUFLQyxXQUFMLEdBQW1CeEIsVUFBbkI7O0FBQ0EsUUFBSXNCLE1BQU0sQ0FBQ0csUUFBWCxFQUFxQjtBQUNuQixXQUFLQyxhQUFMLENBQW1CSixNQUFNLENBQUNHLFFBQTFCO0FBQ0Q7O0FBQ0QsU0FBS0UsY0FBTCxHQUFzQixLQUFLQSxjQUFMLENBQW9CQyxJQUFwQixDQUF5QixJQUF6QixDQUF0QjtBQUNBLFNBQUtDLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxTQUFLQyxVQUFMLEdBQWtCbEMscUJBQXFCLENBQUMwQixNQUFELENBQXZDOztBQUVBLFFBQUlTLE9BQUosRUFBYTtBQUNYLFdBQUtDLGdCQUFMO0FBQ0Q7QUFDRjs7OztrQ0FFYUMsUSxFQUEwQjtBQUN0QyxXQUFLVixVQUFMLENBQWdCakIsSUFBaEIsQ0FBcUIyQixRQUFyQjtBQUNEOzs7cUNBRWdCQSxRLEVBQTBCO0FBQ3pDLFdBQUtWLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxDQUFnQlcsTUFBaEIsQ0FBdUIsVUFBQVQsUUFBUTtBQUFBLGVBQUlBLFFBQVEsS0FBS1EsUUFBakI7QUFBQSxPQUEvQixDQUFsQjtBQUNEOzs7NkJBRVFuQyxPLEVBQWNDLFMsRUFBbUI7QUFDeENKLE1BQUFBLFNBQVMsQ0FDUCxLQUFLbUMsVUFERSxFQUVQLGdEQUZPLENBQVQ7QUFLQSxXQUFLRCxjQUFMLEdBQXNCaEMsaUJBQWlCLENBQ3JDQyxPQURxQyxFQUVyQ0MsU0FGcUMsRUFHckMsS0FBS3lCLFdBSGdDLENBQXZDO0FBS0Q7Ozs2QkFFUVgsTyxFQUFjZCxTLEVBQW1CO0FBQ3hDSixNQUFBQSxTQUFTLENBQ1AsS0FBS21DLFVBREUsRUFFUCxnREFGTyxDQUFUO0FBS0EsV0FBS0QsY0FBTCxJQUF1QixLQUFLQSxjQUFMLENBQW9CVixNQUFwQixFQUF2QjtBQUNEOzs7bUNBRWM7QUFBQTs7QUFDYixVQUFJLEtBQUtXLFVBQVQsRUFBcUI7QUFDbkIsZUFBTyxLQUFLSCxjQUFaO0FBQ0Q7O0FBRUQsYUFBTyxZQUFrQjtBQUFBLDBDQUFkUSxJQUFjO0FBQWRBLFVBQUFBLElBQWM7QUFBQTs7QUFDdkIsWUFBTWpDLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQUNrQyxVQUFELEVBQWFDLE1BQWIsRUFBcUIzQixHQUFyQixFQUE2QjtBQUM1QyxjQUFJLE9BQU8yQixNQUFQLEtBQWtCLFFBQWxCLElBQThCRCxVQUFVLFlBQVk3QyxhQUF4RCxFQUF1RTtBQUNyRTZDLFlBQUFBLFVBQVUsQ0FBQ0UsUUFBWCxDQUFvQkQsTUFBcEI7QUFDRCxXQUZELE1BRU8sSUFBSSxPQUFPRCxVQUFQLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ3pDLGlCQUFLLElBQU1HLFVBQVgsSUFBeUJILFVBQXpCLEVBQXFDO0FBSW5DbEMsY0FBQUEsUUFBUSxDQUFDa0MsVUFBVSxDQUFDRyxVQUFELENBQVgsRUFBeUJGLE1BQU0sQ0FBQ0UsVUFBRCxDQUEvQixFQUE2Q0EsVUFBN0MsQ0FBUjtBQUNEO0FBQ0Y7QUFDRixTQVhEOztBQWFBLFlBQUksQ0FBQyxLQUFJLENBQUNULFVBQVYsRUFBc0I7QUFDcEIsVUFBQSxLQUFJLENBQUNOLFdBQUwsQ0FBaUJULE9BQWpCLENBQXlCLFVBQUNDLE9BQUQsRUFBVXdCLEdBQVYsRUFBa0I7QUFDekN0QyxZQUFBQSxRQUFRLENBQUNjLE9BQUQsRUFBVW1CLElBQUksQ0FBQ0ssR0FBRCxDQUFkLEVBQXFCLFFBQVFBLEdBQTdCLENBQVI7QUFDRCxXQUZEO0FBR0Q7O0FBQ0QsUUFBQSxLQUFJLENBQUNiLGNBQUwsT0FBQSxLQUFJLEVBQW1CUSxJQUFuQixDQUFKO0FBQ0QsT0FwQkQ7QUFxQkQ7OztxQ0FFdUI7QUFBQSx5Q0FBTkEsSUFBTTtBQUFOQSxRQUFBQSxJQUFNO0FBQUE7O0FBQ3RCLFdBQUtaLFVBQUwsQ0FBZ0JSLE9BQWhCLENBQXdCLFVBQUFVLFFBQVE7QUFBQSxlQUFJQSxRQUFRLE1BQVIsU0FBWVUsSUFBWixDQUFKO0FBQUEsT0FBaEM7QUFDRDs7O3VDQUVrQjtBQUNqQixVQUFNakMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ2tDLFVBQUQsRUFBYUMsTUFBYixFQUFxQjNCLEdBQXJCLEVBQTZCO0FBQzVDLFlBQUksT0FBTzJCLE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIxQyxVQUFBQSxTQUFTLENBQ1B5QyxVQUFVLFlBQVk3QyxhQURmLEVBRVAseUJBQ0UsT0FBTzZDLFVBRFQsR0FFRSxXQUZGLEdBR0UxQixHQUhGLEdBSUUseUNBTkssQ0FBVDtBQVFBO0FBQ0Q7O0FBQ0RmLFFBQUFBLFNBQVMsQ0FDUCxPQUFPeUMsVUFBUCxLQUFzQixRQURmLEVBRVAseUJBQXlCLE9BQU9BLFVBQWhDLEdBQTZDLFdBQTdDLEdBQTJEMUIsR0FGcEQsQ0FBVDtBQUlBZixRQUFBQSxTQUFTLENBQ1AsT0FBTzBDLE1BQVAsS0FBa0IsUUFEWCxFQUVQLHVCQUF1QixPQUFPQSxNQUE5QixHQUF1QyxXQUF2QyxHQUFxRDNCLEdBRjlDLENBQVQ7O0FBSUEsYUFBSyxJQUFNNkIsVUFBWCxJQUF5QkgsVUFBekIsRUFBcUM7QUFDbkNsQyxVQUFBQSxRQUFRLENBQUNrQyxVQUFVLENBQUNHLFVBQUQsQ0FBWCxFQUF5QkYsTUFBTSxDQUFDRSxVQUFELENBQS9CLEVBQTZDQSxVQUE3QyxDQUFSO0FBQ0Q7QUFDRixPQXZCRDtBQXdCRDs7Ozs7QUFHSEUsTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQUNyQixFQUFBQSxhQUFhLEVBQWJBLGFBQUQ7QUFBZ0J4QixFQUFBQSxpQkFBaUIsRUFBakJBO0FBQWhCLENBQWpCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTUtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKiBAZmxvd1xuICogQGZvcm1hdFxuICovXG4ndXNlIHN0cmljdCc7XG5cbmNvbnN0IEFuaW1hdGVkVmFsdWUgPSByZXF1aXJlKCcuL25vZGVzL0FuaW1hdGVkVmFsdWUnKTtcbmNvbnN0IE5hdGl2ZUFuaW1hdGVkSGVscGVyID0gcmVxdWlyZSgnLi9OYXRpdmVBbmltYXRlZEhlbHBlcicpO1xuY29uc3QgUmVhY3ROYXRpdmUgPSByZXF1aXJlKCcuLi8uLi9SZW5kZXJlci9zaGltcy9SZWFjdE5hdGl2ZScpO1xuXG5jb25zdCBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcbmNvbnN0IHtzaG91bGRVc2VOYXRpdmVEcml2ZXJ9ID0gcmVxdWlyZSgnLi9OYXRpdmVBbmltYXRlZEhlbHBlcicpO1xuXG5leHBvcnQgdHlwZSBNYXBwaW5nID0ge1trZXk6IHN0cmluZ106IE1hcHBpbmd9IHwgQW5pbWF0ZWRWYWx1ZTtcbmV4cG9ydCB0eXBlIEV2ZW50Q29uZmlnID0ge1xuICBsaXN0ZW5lcj86ID9GdW5jdGlvbixcbiAgdXNlTmF0aXZlRHJpdmVyPzogYm9vbGVhbixcbn07XG5cbmZ1bmN0aW9uIGF0dGFjaE5hdGl2ZUV2ZW50KFxuICB2aWV3UmVmOiBhbnksXG4gIGV2ZW50TmFtZTogc3RyaW5nLFxuICBhcmdNYXBwaW5nOiBBcnJheTw/TWFwcGluZz4sXG4pIHtcbiAgLy8gRmluZCBhbmltYXRlZCB2YWx1ZXMgaW4gYGFyZ01hcHBpbmdgIGFuZCBjcmVhdGUgYW4gYXJyYXkgcmVwcmVzZW50aW5nIHRoZWlyXG4gIC8vIGtleSBwYXRoIGluc2lkZSB0aGUgYG5hdGl2ZUV2ZW50YCBvYmplY3QuIEV4LjogWydjb250ZW50T2Zmc2V0JywgJ3gnXS5cbiAgY29uc3QgZXZlbnRNYXBwaW5ncyA9IFtdO1xuXG4gIGNvbnN0IHRyYXZlcnNlID0gKHZhbHVlLCBwYXRoKSA9PiB7XG4gICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgQW5pbWF0ZWRWYWx1ZSkge1xuICAgICAgdmFsdWUuX19tYWtlTmF0aXZlKCk7XG5cbiAgICAgIGV2ZW50TWFwcGluZ3MucHVzaCh7XG4gICAgICAgIG5hdGl2ZUV2ZW50UGF0aDogcGF0aCxcbiAgICAgICAgYW5pbWF0ZWRWYWx1ZVRhZzogdmFsdWUuX19nZXROYXRpdmVUYWcoKSxcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gdmFsdWUpIHtcbiAgICAgICAgdHJhdmVyc2UodmFsdWVba2V5XSwgcGF0aC5jb25jYXQoa2V5KSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIGludmFyaWFudChcbiAgICBhcmdNYXBwaW5nWzBdICYmIGFyZ01hcHBpbmdbMF0ubmF0aXZlRXZlbnQsXG4gICAgJ05hdGl2ZSBkcml2ZW4gZXZlbnRzIG9ubHkgc3VwcG9ydCBhbmltYXRlZCB2YWx1ZXMgY29udGFpbmVkIGluc2lkZSBgbmF0aXZlRXZlbnRgLicsXG4gICk7XG5cbiAgLy8gQXNzdW1lIHRoYXQgdGhlIGV2ZW50IGNvbnRhaW5pbmcgYG5hdGl2ZUV2ZW50YCBpcyBhbHdheXMgdGhlIGZpcnN0IGFyZ3VtZW50LlxuICB0cmF2ZXJzZShhcmdNYXBwaW5nWzBdLm5hdGl2ZUV2ZW50LCBbXSk7XG5cbiAgY29uc3Qgdmlld1RhZyA9IFJlYWN0TmF0aXZlLmZpbmROb2RlSGFuZGxlKHZpZXdSZWYpO1xuXG4gIGV2ZW50TWFwcGluZ3MuZm9yRWFjaChtYXBwaW5nID0+IHtcbiAgICBOYXRpdmVBbmltYXRlZEhlbHBlci5BUEkuYWRkQW5pbWF0ZWRFdmVudFRvVmlldyhcbiAgICAgIHZpZXdUYWcsXG4gICAgICBldmVudE5hbWUsXG4gICAgICBtYXBwaW5nLFxuICAgICk7XG4gIH0pO1xuXG4gIHJldHVybiB7XG4gICAgZGV0YWNoKCkge1xuICAgICAgZXZlbnRNYXBwaW5ncy5mb3JFYWNoKG1hcHBpbmcgPT4ge1xuICAgICAgICBOYXRpdmVBbmltYXRlZEhlbHBlci5BUEkucmVtb3ZlQW5pbWF0ZWRFdmVudEZyb21WaWV3KFxuICAgICAgICAgIHZpZXdUYWcsXG4gICAgICAgICAgZXZlbnROYW1lLFxuICAgICAgICAgIG1hcHBpbmcuYW5pbWF0ZWRWYWx1ZVRhZyxcbiAgICAgICAgKTtcbiAgICAgIH0pO1xuICAgIH0sXG4gIH07XG59XG5cbmNsYXNzIEFuaW1hdGVkRXZlbnQge1xuICBfYXJnTWFwcGluZzogQXJyYXk8P01hcHBpbmc+O1xuICBfbGlzdGVuZXJzOiBBcnJheTxGdW5jdGlvbj4gPSBbXTtcbiAgX2NhbGxMaXN0ZW5lcnM6IEZ1bmN0aW9uO1xuICBfYXR0YWNoZWRFdmVudDogP3tcbiAgICBkZXRhY2g6ICgpID0+IHZvaWQsXG4gIH07XG4gIF9faXNOYXRpdmU6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoYXJnTWFwcGluZzogQXJyYXk8P01hcHBpbmc+LCBjb25maWc/OiBFdmVudENvbmZpZyA9IHt9KSB7XG4gICAgdGhpcy5fYXJnTWFwcGluZyA9IGFyZ01hcHBpbmc7XG4gICAgaWYgKGNvbmZpZy5saXN0ZW5lcikge1xuICAgICAgdGhpcy5fX2FkZExpc3RlbmVyKGNvbmZpZy5saXN0ZW5lcik7XG4gICAgfVxuICAgIHRoaXMuX2NhbGxMaXN0ZW5lcnMgPSB0aGlzLl9jYWxsTGlzdGVuZXJzLmJpbmQodGhpcyk7XG4gICAgdGhpcy5fYXR0YWNoZWRFdmVudCA9IG51bGw7XG4gICAgdGhpcy5fX2lzTmF0aXZlID0gc2hvdWxkVXNlTmF0aXZlRHJpdmVyKGNvbmZpZyk7XG5cbiAgICBpZiAoX19ERVZfXykge1xuICAgICAgdGhpcy5fdmFsaWRhdGVNYXBwaW5nKCk7XG4gICAgfVxuICB9XG5cbiAgX19hZGRMaXN0ZW5lcihjYWxsYmFjazogRnVuY3Rpb24pOiB2b2lkIHtcbiAgICB0aGlzLl9saXN0ZW5lcnMucHVzaChjYWxsYmFjayk7XG4gIH1cblxuICBfX3JlbW92ZUxpc3RlbmVyKGNhbGxiYWNrOiBGdW5jdGlvbik6IHZvaWQge1xuICAgIHRoaXMuX2xpc3RlbmVycyA9IHRoaXMuX2xpc3RlbmVycy5maWx0ZXIobGlzdGVuZXIgPT4gbGlzdGVuZXIgIT09IGNhbGxiYWNrKTtcbiAgfVxuXG4gIF9fYXR0YWNoKHZpZXdSZWY6IGFueSwgZXZlbnROYW1lOiBzdHJpbmcpIHtcbiAgICBpbnZhcmlhbnQoXG4gICAgICB0aGlzLl9faXNOYXRpdmUsXG4gICAgICAnT25seSBuYXRpdmUgZHJpdmVuIGV2ZW50cyBuZWVkIHRvIGJlIGF0dGFjaGVkLicsXG4gICAgKTtcblxuICAgIHRoaXMuX2F0dGFjaGVkRXZlbnQgPSBhdHRhY2hOYXRpdmVFdmVudChcbiAgICAgIHZpZXdSZWYsXG4gICAgICBldmVudE5hbWUsXG4gICAgICB0aGlzLl9hcmdNYXBwaW5nLFxuICAgICk7XG4gIH1cblxuICBfX2RldGFjaCh2aWV3VGFnOiBhbnksIGV2ZW50TmFtZTogc3RyaW5nKSB7XG4gICAgaW52YXJpYW50KFxuICAgICAgdGhpcy5fX2lzTmF0aXZlLFxuICAgICAgJ09ubHkgbmF0aXZlIGRyaXZlbiBldmVudHMgbmVlZCB0byBiZSBkZXRhY2hlZC4nLFxuICAgICk7XG5cbiAgICB0aGlzLl9hdHRhY2hlZEV2ZW50ICYmIHRoaXMuX2F0dGFjaGVkRXZlbnQuZGV0YWNoKCk7XG4gIH1cblxuICBfX2dldEhhbmRsZXIoKSB7XG4gICAgaWYgKHRoaXMuX19pc05hdGl2ZSkge1xuICAgICAgcmV0dXJuIHRoaXMuX2NhbGxMaXN0ZW5lcnM7XG4gICAgfVxuXG4gICAgcmV0dXJuICguLi5hcmdzOiBhbnkpID0+IHtcbiAgICAgIGNvbnN0IHRyYXZlcnNlID0gKHJlY01hcHBpbmcsIHJlY0V2dCwga2V5KSA9PiB7XG4gICAgICAgIGlmICh0eXBlb2YgcmVjRXZ0ID09PSAnbnVtYmVyJyAmJiByZWNNYXBwaW5nIGluc3RhbmNlb2YgQW5pbWF0ZWRWYWx1ZSkge1xuICAgICAgICAgIHJlY01hcHBpbmcuc2V0VmFsdWUocmVjRXZ0KTtcbiAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgcmVjTWFwcGluZyA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICBmb3IgKGNvbnN0IG1hcHBpbmdLZXkgaW4gcmVjTWFwcGluZykge1xuICAgICAgICAgICAgLyogJEZsb3dGaXhNZSg+PTAuNTMuMCBzaXRlPXJlYWN0X25hdGl2ZV9mYixyZWFjdF9uYXRpdmVfb3NzKSBUaGlzXG4gICAgICAgICAgICAgKiBjb21tZW50IHN1cHByZXNzZXMgYW4gZXJyb3Igd2hlbiB1cGdyYWRpbmcgRmxvdydzIHN1cHBvcnQgZm9yXG4gICAgICAgICAgICAgKiBSZWFjdC4gVG8gc2VlIHRoZSBlcnJvciBkZWxldGUgdGhpcyBjb21tZW50IGFuZCBydW4gRmxvdy4gKi9cbiAgICAgICAgICAgIHRyYXZlcnNlKHJlY01hcHBpbmdbbWFwcGluZ0tleV0sIHJlY0V2dFttYXBwaW5nS2V5XSwgbWFwcGluZ0tleSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZiAoIXRoaXMuX19pc05hdGl2ZSkge1xuICAgICAgICB0aGlzLl9hcmdNYXBwaW5nLmZvckVhY2goKG1hcHBpbmcsIGlkeCkgPT4ge1xuICAgICAgICAgIHRyYXZlcnNlKG1hcHBpbmcsIGFyZ3NbaWR4XSwgJ2FyZycgKyBpZHgpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHRoaXMuX2NhbGxMaXN0ZW5lcnMoLi4uYXJncyk7XG4gICAgfTtcbiAgfVxuXG4gIF9jYWxsTGlzdGVuZXJzKC4uLmFyZ3MpIHtcbiAgICB0aGlzLl9saXN0ZW5lcnMuZm9yRWFjaChsaXN0ZW5lciA9PiBsaXN0ZW5lciguLi5hcmdzKSk7XG4gIH1cblxuICBfdmFsaWRhdGVNYXBwaW5nKCkge1xuICAgIGNvbnN0IHRyYXZlcnNlID0gKHJlY01hcHBpbmcsIHJlY0V2dCwga2V5KSA9PiB7XG4gICAgICBpZiAodHlwZW9mIHJlY0V2dCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgaW52YXJpYW50KFxuICAgICAgICAgIHJlY01hcHBpbmcgaW5zdGFuY2VvZiBBbmltYXRlZFZhbHVlLFxuICAgICAgICAgICdCYWQgbWFwcGluZyBvZiB0eXBlICcgK1xuICAgICAgICAgICAgdHlwZW9mIHJlY01hcHBpbmcgK1xuICAgICAgICAgICAgJyBmb3Iga2V5ICcgK1xuICAgICAgICAgICAga2V5ICtcbiAgICAgICAgICAgICcsIGV2ZW50IHZhbHVlIG11c3QgbWFwIHRvIEFuaW1hdGVkVmFsdWUnLFxuICAgICAgICApO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpbnZhcmlhbnQoXG4gICAgICAgIHR5cGVvZiByZWNNYXBwaW5nID09PSAnb2JqZWN0JyxcbiAgICAgICAgJ0JhZCBtYXBwaW5nIG9mIHR5cGUgJyArIHR5cGVvZiByZWNNYXBwaW5nICsgJyBmb3Iga2V5ICcgKyBrZXksXG4gICAgICApO1xuICAgICAgaW52YXJpYW50KFxuICAgICAgICB0eXBlb2YgcmVjRXZ0ID09PSAnb2JqZWN0JyxcbiAgICAgICAgJ0JhZCBldmVudCBvZiB0eXBlICcgKyB0eXBlb2YgcmVjRXZ0ICsgJyBmb3Iga2V5ICcgKyBrZXksXG4gICAgICApO1xuICAgICAgZm9yIChjb25zdCBtYXBwaW5nS2V5IGluIHJlY01hcHBpbmcpIHtcbiAgICAgICAgdHJhdmVyc2UocmVjTWFwcGluZ1ttYXBwaW5nS2V5XSwgcmVjRXZ0W21hcHBpbmdLZXldLCBtYXBwaW5nS2V5KTtcbiAgICAgIH1cbiAgICB9O1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge0FuaW1hdGVkRXZlbnQsIGF0dGFjaE5hdGl2ZUV2ZW50fTtcbiJdfQ==