f7d3a380e61ed2510545f30ec65612d4
'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _require = require('./AnimatedEvent'),
    AnimatedEvent = _require.AnimatedEvent;

var AnimatedProps = require('./nodes/AnimatedProps');

var React = require('react');

var ViewStylePropTypes = require('../../Components/View/ViewStylePropTypes');

var invariant = require('fbjs/lib/invariant');

function createAnimatedComponent(Component) {
  invariant(typeof Component !== 'function' || Component.prototype && Component.prototype.isReactComponent, '`createAnimatedComponent` does not support stateless functional components; ' + 'use a class component instead.');

  var AnimatedComponent = function (_React$Component) {
    (0, _inherits2.default)(AnimatedComponent, _React$Component);

    function AnimatedComponent(props) {
      var _this;

      (0, _classCallCheck2.default)(this, AnimatedComponent);
      _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(AnimatedComponent).call(this, props));
      _this._invokeAnimatedPropsCallbackOnMount = false;
      _this._eventDetachers = [];

      _this._animatedPropsCallback = function () {
        if (_this._component == null) {
          _this._invokeAnimatedPropsCallbackOnMount = true;
        } else if (AnimatedComponent.__skipSetNativeProps_FOR_TESTS_ONLY || typeof _this._component.setNativeProps !== 'function') {
          _this.forceUpdate();
        } else if (!_this._propsAnimated.__isNative) {
          _this._component.setNativeProps(_this._propsAnimated.__getAnimatedValue());
        } else {
          throw new Error('Attempting to run JS driven animation on animated ' + 'node that has been moved to "native" earlier by starting an ' + 'animation with `useNativeDriver: true`');
        }
      };

      _this._setComponentRef = _this._setComponentRef.bind((0, _assertThisInitialized2.default)(_this));
      return _this;
    }

    (0, _createClass2.default)(AnimatedComponent, [{
      key: "componentWillUnmount",
      value: function componentWillUnmount() {
        this._propsAnimated && this._propsAnimated.__detach();

        this._detachNativeEvents();
      }
    }, {
      key: "setNativeProps",
      value: function setNativeProps(props) {
        this._component.setNativeProps(props);
      }
    }, {
      key: "UNSAFE_componentWillMount",
      value: function UNSAFE_componentWillMount() {
        this._attachProps(this.props);
      }
    }, {
      key: "componentDidMount",
      value: function componentDidMount() {
        if (this._invokeAnimatedPropsCallbackOnMount) {
          this._invokeAnimatedPropsCallbackOnMount = false;

          this._animatedPropsCallback();
        }

        this._propsAnimated.setNativeView(this._component);

        this._attachNativeEvents();
      }
    }, {
      key: "_attachNativeEvents",
      value: function _attachNativeEvents() {
        var _this2 = this;

        var scrollableNode = this._component.getScrollableNode ? this._component.getScrollableNode() : this._component;

        var _loop = function _loop(key) {
          var prop = _this2.props[key];

          if (prop instanceof AnimatedEvent && prop.__isNative) {
            prop.__attach(scrollableNode, key);

            _this2._eventDetachers.push(function () {
              return prop.__detach(scrollableNode, key);
            });
          }
        };

        for (var key in this.props) {
          _loop(key);
        }
      }
    }, {
      key: "_detachNativeEvents",
      value: function _detachNativeEvents() {
        this._eventDetachers.forEach(function (remove) {
          return remove();
        });

        this._eventDetachers = [];
      }
    }, {
      key: "_attachProps",
      value: function _attachProps(nextProps) {
        var oldPropsAnimated = this._propsAnimated;
        this._propsAnimated = new AnimatedProps(nextProps, this._animatedPropsCallback);
        oldPropsAnimated && oldPropsAnimated.__detach();
      }
    }, {
      key: "UNSAFE_componentWillReceiveProps",
      value: function UNSAFE_componentWillReceiveProps(newProps) {
        this._attachProps(newProps);
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps) {
        if (this._component !== this._prevComponent) {
          this._propsAnimated.setNativeView(this._component);
        }

        if (this._component !== this._prevComponent || prevProps !== this.props) {
          this._detachNativeEvents();

          this._attachNativeEvents();
        }
      }
    }, {
      key: "render",
      value: function render() {
        var props = this._propsAnimated.__getValue();

        return React.createElement(Component, (0, _extends2.default)({}, props, {
          ref: this._setComponentRef,
          collapsable: this._propsAnimated.__isNative ? false : props.collapsable
        }));
      }
    }, {
      key: "_setComponentRef",
      value: function _setComponentRef(c) {
        this._prevComponent = this._component;
        this._component = c;
      }
    }, {
      key: "getNode",
      value: function getNode() {
        return this._component;
      }
    }]);
    return AnimatedComponent;
  }(React.Component);

  AnimatedComponent.__skipSetNativeProps_FOR_TESTS_ONLY = false;
  var propTypes = Component.propTypes;
  AnimatedComponent.propTypes = {
    style: function style(props, propName, componentName) {
      if (!propTypes) {
        return;
      }

      for (var key in ViewStylePropTypes) {
        if (!propTypes[key] && props[key] !== undefined) {
          console.warn('You are setting the style `{ ' + key + ': ... }` as a prop. You ' + 'should nest it in a style object. ' + 'E.g. `{ style: { ' + key + ': ... } }`');
        }
      }
    }
  };
  return AnimatedComponent;
}

module.exports = createAnimatedComponent;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNyZWF0ZUFuaW1hdGVkQ29tcG9uZW50LmpzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCJBbmltYXRlZEV2ZW50IiwiQW5pbWF0ZWRQcm9wcyIsIlJlYWN0IiwiVmlld1N0eWxlUHJvcFR5cGVzIiwiaW52YXJpYW50IiwiY3JlYXRlQW5pbWF0ZWRDb21wb25lbnQiLCJDb21wb25lbnQiLCJwcm90b3R5cGUiLCJpc1JlYWN0Q29tcG9uZW50IiwiQW5pbWF0ZWRDb21wb25lbnQiLCJwcm9wcyIsIl9pbnZva2VBbmltYXRlZFByb3BzQ2FsbGJhY2tPbk1vdW50IiwiX2V2ZW50RGV0YWNoZXJzIiwiX2FuaW1hdGVkUHJvcHNDYWxsYmFjayIsIl9jb21wb25lbnQiLCJfX3NraXBTZXROYXRpdmVQcm9wc19GT1JfVEVTVFNfT05MWSIsInNldE5hdGl2ZVByb3BzIiwiZm9yY2VVcGRhdGUiLCJfcHJvcHNBbmltYXRlZCIsIl9faXNOYXRpdmUiLCJfX2dldEFuaW1hdGVkVmFsdWUiLCJFcnJvciIsIl9zZXRDb21wb25lbnRSZWYiLCJiaW5kIiwiX19kZXRhY2giLCJfZGV0YWNoTmF0aXZlRXZlbnRzIiwiX2F0dGFjaFByb3BzIiwic2V0TmF0aXZlVmlldyIsIl9hdHRhY2hOYXRpdmVFdmVudHMiLCJzY3JvbGxhYmxlTm9kZSIsImdldFNjcm9sbGFibGVOb2RlIiwia2V5IiwicHJvcCIsIl9fYXR0YWNoIiwicHVzaCIsImZvckVhY2giLCJyZW1vdmUiLCJuZXh0UHJvcHMiLCJvbGRQcm9wc0FuaW1hdGVkIiwibmV3UHJvcHMiLCJwcmV2UHJvcHMiLCJfcHJldkNvbXBvbmVudCIsIl9fZ2V0VmFsdWUiLCJjb2xsYXBzYWJsZSIsImMiLCJwcm9wVHlwZXMiLCJzdHlsZSIsInByb3BOYW1lIiwiY29tcG9uZW50TmFtZSIsInVuZGVmaW5lZCIsImNvbnNvbGUiLCJ3YXJuIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6IkFBU0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztlQUV3QkEsT0FBTyxDQUFDLGlCQUFELEM7SUFBeEJDLGEsWUFBQUEsYTs7QUFDUCxJQUFNQyxhQUFhLEdBQUdGLE9BQU8sQ0FBQyx1QkFBRCxDQUE3Qjs7QUFDQSxJQUFNRyxLQUFLLEdBQUdILE9BQU8sQ0FBQyxPQUFELENBQXJCOztBQUNBLElBQU1JLGtCQUFrQixHQUFHSixPQUFPLENBQUMsMENBQUQsQ0FBbEM7O0FBRUEsSUFBTUssU0FBUyxHQUFHTCxPQUFPLENBQUMsb0JBQUQsQ0FBekI7O0FBRUEsU0FBU00sdUJBQVQsQ0FBaUNDLFNBQWpDLEVBQXNEO0FBQ3BERixFQUFBQSxTQUFTLENBQ1AsT0FBT0UsU0FBUCxLQUFxQixVQUFyQixJQUNHQSxTQUFTLENBQUNDLFNBQVYsSUFBdUJELFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsZ0JBRnZDLEVBR1AsaUZBQ0UsZ0NBSkssQ0FBVDs7QUFEb0QsTUFROUNDLGlCQVI4QztBQUFBOztBQWtCbEQsK0JBQVlDLEtBQVosRUFBMkI7QUFBQTs7QUFBQTtBQUN6Qix5SEFBTUEsS0FBTjtBQUR5QixZQVIzQkMsbUNBUTJCLEdBUm9CLEtBUXBCO0FBQUEsWUFMM0JDLGVBSzJCLEdBTFEsRUFLUjs7QUFBQSxZQXNEM0JDLHNCQXREMkIsR0FzREYsWUFBTTtBQUM3QixZQUFJLE1BQUtDLFVBQUwsSUFBbUIsSUFBdkIsRUFBNkI7QUFNM0IsZ0JBQUtILG1DQUFMLEdBQTJDLElBQTNDO0FBQ0QsU0FQRCxNQU9PLElBQ0xGLGlCQUFpQixDQUFDTSxtQ0FBbEIsSUFDQSxPQUFPLE1BQUtELFVBQUwsQ0FBZ0JFLGNBQXZCLEtBQTBDLFVBRnJDLEVBR0w7QUFDQSxnQkFBS0MsV0FBTDtBQUNELFNBTE0sTUFLQSxJQUFJLENBQUMsTUFBS0MsY0FBTCxDQUFvQkMsVUFBekIsRUFBcUM7QUFDMUMsZ0JBQUtMLFVBQUwsQ0FBZ0JFLGNBQWhCLENBQ0UsTUFBS0UsY0FBTCxDQUFvQkUsa0JBQXBCLEVBREY7QUFHRCxTQUpNLE1BSUE7QUFDTCxnQkFBTSxJQUFJQyxLQUFKLENBQ0osdURBQ0UsOERBREYsR0FFRSx3Q0FIRSxDQUFOO0FBS0Q7QUFDRixPQTlFMEI7O0FBRXpCLFlBQUtDLGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCQyxJQUF0Qiw2Q0FBeEI7QUFGeUI7QUFHMUI7O0FBckJpRDtBQUFBO0FBQUEsNkNBdUIzQjtBQUNyQixhQUFLTCxjQUFMLElBQXVCLEtBQUtBLGNBQUwsQ0FBb0JNLFFBQXBCLEVBQXZCOztBQUNBLGFBQUtDLG1CQUFMO0FBQ0Q7QUExQmlEO0FBQUE7QUFBQSxxQ0E0Qm5DZixLQTVCbUMsRUE0QjVCO0FBQ3BCLGFBQUtJLFVBQUwsQ0FBZ0JFLGNBQWhCLENBQStCTixLQUEvQjtBQUNEO0FBOUJpRDtBQUFBO0FBQUEsa0RBZ0N0QjtBQUMxQixhQUFLZ0IsWUFBTCxDQUFrQixLQUFLaEIsS0FBdkI7QUFDRDtBQWxDaUQ7QUFBQTtBQUFBLDBDQW9DOUI7QUFDbEIsWUFBSSxLQUFLQyxtQ0FBVCxFQUE4QztBQUM1QyxlQUFLQSxtQ0FBTCxHQUEyQyxLQUEzQzs7QUFDQSxlQUFLRSxzQkFBTDtBQUNEOztBQUVELGFBQUtLLGNBQUwsQ0FBb0JTLGFBQXBCLENBQWtDLEtBQUtiLFVBQXZDOztBQUNBLGFBQUtjLG1CQUFMO0FBQ0Q7QUE1Q2lEO0FBQUE7QUFBQSw0Q0E4QzVCO0FBQUE7O0FBR3BCLFlBQU1DLGNBQWMsR0FBRyxLQUFLZixVQUFMLENBQWdCZ0IsaUJBQWhCLEdBQ25CLEtBQUtoQixVQUFMLENBQWdCZ0IsaUJBQWhCLEVBRG1CLEdBRW5CLEtBQUtoQixVQUZUOztBQUhvQixtQ0FPVGlCLEdBUFM7QUFRbEIsY0FBTUMsSUFBSSxHQUFHLE1BQUksQ0FBQ3RCLEtBQUwsQ0FBV3FCLEdBQVgsQ0FBYjs7QUFDQSxjQUFJQyxJQUFJLFlBQVloQyxhQUFoQixJQUFpQ2dDLElBQUksQ0FBQ2IsVUFBMUMsRUFBc0Q7QUFDcERhLFlBQUFBLElBQUksQ0FBQ0MsUUFBTCxDQUFjSixjQUFkLEVBQThCRSxHQUE5Qjs7QUFDQSxZQUFBLE1BQUksQ0FBQ25CLGVBQUwsQ0FBcUJzQixJQUFyQixDQUEwQjtBQUFBLHFCQUFNRixJQUFJLENBQUNSLFFBQUwsQ0FBY0ssY0FBZCxFQUE4QkUsR0FBOUIsQ0FBTjtBQUFBLGFBQTFCO0FBQ0Q7QUFaaUI7O0FBT3BCLGFBQUssSUFBTUEsR0FBWCxJQUFrQixLQUFLckIsS0FBdkIsRUFBOEI7QUFBQSxnQkFBbkJxQixHQUFtQjtBQU03QjtBQUNGO0FBNURpRDtBQUFBO0FBQUEsNENBOEQ1QjtBQUNwQixhQUFLbkIsZUFBTCxDQUFxQnVCLE9BQXJCLENBQTZCLFVBQUFDLE1BQU07QUFBQSxpQkFBSUEsTUFBTSxFQUFWO0FBQUEsU0FBbkM7O0FBQ0EsYUFBS3hCLGVBQUwsR0FBdUIsRUFBdkI7QUFDRDtBQWpFaUQ7QUFBQTtBQUFBLG1DQWtHckN5QixTQWxHcUMsRUFrRzFCO0FBQ3RCLFlBQU1DLGdCQUFnQixHQUFHLEtBQUtwQixjQUE5QjtBQUVBLGFBQUtBLGNBQUwsR0FBc0IsSUFBSWpCLGFBQUosQ0FDcEJvQyxTQURvQixFQUVwQixLQUFLeEIsc0JBRmUsQ0FBdEI7QUFhQXlCLFFBQUFBLGdCQUFnQixJQUFJQSxnQkFBZ0IsQ0FBQ2QsUUFBakIsRUFBcEI7QUFDRDtBQW5IaUQ7QUFBQTtBQUFBLHVEQXFIakJlLFFBckhpQixFQXFIUDtBQUN6QyxhQUFLYixZQUFMLENBQWtCYSxRQUFsQjtBQUNEO0FBdkhpRDtBQUFBO0FBQUEseUNBeUgvQkMsU0F6SCtCLEVBeUhwQjtBQUM1QixZQUFJLEtBQUsxQixVQUFMLEtBQW9CLEtBQUsyQixjQUE3QixFQUE2QztBQUMzQyxlQUFLdkIsY0FBTCxDQUFvQlMsYUFBcEIsQ0FBa0MsS0FBS2IsVUFBdkM7QUFDRDs7QUFDRCxZQUFJLEtBQUtBLFVBQUwsS0FBb0IsS0FBSzJCLGNBQXpCLElBQTJDRCxTQUFTLEtBQUssS0FBSzlCLEtBQWxFLEVBQXlFO0FBQ3ZFLGVBQUtlLG1CQUFMOztBQUNBLGVBQUtHLG1CQUFMO0FBQ0Q7QUFDRjtBQWpJaUQ7QUFBQTtBQUFBLCtCQW1JekM7QUFDUCxZQUFNbEIsS0FBSyxHQUFHLEtBQUtRLGNBQUwsQ0FBb0J3QixVQUFwQixFQUFkOztBQUNBLGVBQ0Usb0JBQUMsU0FBRCw2QkFDTWhDLEtBRE47QUFFRSxVQUFBLEdBQUcsRUFBRSxLQUFLWSxnQkFGWjtBQU9FLFVBQUEsV0FBVyxFQUNULEtBQUtKLGNBQUwsQ0FBb0JDLFVBQXBCLEdBQWlDLEtBQWpDLEdBQXlDVCxLQUFLLENBQUNpQztBQVJuRCxXQURGO0FBYUQ7QUFsSmlEO0FBQUE7QUFBQSx1Q0FvSmpDQyxDQXBKaUMsRUFvSjlCO0FBQ2xCLGFBQUtILGNBQUwsR0FBc0IsS0FBSzNCLFVBQTNCO0FBQ0EsYUFBS0EsVUFBTCxHQUFrQjhCLENBQWxCO0FBQ0Q7QUF2SmlEO0FBQUE7QUFBQSxnQ0EySnhDO0FBQ1IsZUFBTyxLQUFLOUIsVUFBWjtBQUNEO0FBN0ppRDtBQUFBO0FBQUEsSUFRcEJaLEtBQUssQ0FBQ0ksU0FSYzs7QUFROUNHLEVBQUFBLGlCQVI4QyxDQWdCM0NNLG1DQWhCMkMsR0FnQkwsS0FoQks7QUFnS3BELE1BQU04QixTQUFTLEdBQUd2QyxTQUFTLENBQUN1QyxTQUE1QjtBQUVBcEMsRUFBQUEsaUJBQWlCLENBQUNvQyxTQUFsQixHQUE4QjtBQUM1QkMsSUFBQUEsS0FBSyxFQUFFLGVBQVNwQyxLQUFULEVBQWdCcUMsUUFBaEIsRUFBMEJDLGFBQTFCLEVBQXlDO0FBQzlDLFVBQUksQ0FBQ0gsU0FBTCxFQUFnQjtBQUNkO0FBQ0Q7O0FBRUQsV0FBSyxJQUFNZCxHQUFYLElBQWtCNUIsa0JBQWxCLEVBQXNDO0FBQ3BDLFlBQUksQ0FBQzBDLFNBQVMsQ0FBQ2QsR0FBRCxDQUFWLElBQW1CckIsS0FBSyxDQUFDcUIsR0FBRCxDQUFMLEtBQWVrQixTQUF0QyxFQUFpRDtBQUMvQ0MsVUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQ0Usa0NBQ0VwQixHQURGLEdBRUUsMEJBRkYsR0FHRSxvQ0FIRixHQUlFLG1CQUpGLEdBS0VBLEdBTEYsR0FNRSxZQVBKO0FBU0Q7QUFDRjtBQUNGO0FBbkIyQixHQUE5QjtBQXNCQSxTQUFPdEIsaUJBQVA7QUFDRDs7QUFFRDJDLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQmhELHVCQUFqQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDE1LXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQGZsb3dcbiAqIEBmb3JtYXRcbiAqL1xuJ3VzZSBzdHJpY3QnO1xuXG5jb25zdCB7QW5pbWF0ZWRFdmVudH0gPSByZXF1aXJlKCcuL0FuaW1hdGVkRXZlbnQnKTtcbmNvbnN0IEFuaW1hdGVkUHJvcHMgPSByZXF1aXJlKCcuL25vZGVzL0FuaW1hdGVkUHJvcHMnKTtcbmNvbnN0IFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcbmNvbnN0IFZpZXdTdHlsZVByb3BUeXBlcyA9IHJlcXVpcmUoJy4uLy4uL0NvbXBvbmVudHMvVmlldy9WaWV3U3R5bGVQcm9wVHlwZXMnKTtcblxuY29uc3QgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbmZ1bmN0aW9uIGNyZWF0ZUFuaW1hdGVkQ29tcG9uZW50KENvbXBvbmVudDogYW55KTogYW55IHtcbiAgaW52YXJpYW50KFxuICAgIHR5cGVvZiBDb21wb25lbnQgIT09ICdmdW5jdGlvbicgfHxcbiAgICAgIChDb21wb25lbnQucHJvdG90eXBlICYmIENvbXBvbmVudC5wcm90b3R5cGUuaXNSZWFjdENvbXBvbmVudCksXG4gICAgJ2BjcmVhdGVBbmltYXRlZENvbXBvbmVudGAgZG9lcyBub3Qgc3VwcG9ydCBzdGF0ZWxlc3MgZnVuY3Rpb25hbCBjb21wb25lbnRzOyAnICtcbiAgICAgICd1c2UgYSBjbGFzcyBjb21wb25lbnQgaW5zdGVhZC4nLFxuICApO1xuXG4gIGNsYXNzIEFuaW1hdGVkQ29tcG9uZW50IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PE9iamVjdD4ge1xuICAgIF9jb21wb25lbnQ6IGFueTtcbiAgICBfaW52b2tlQW5pbWF0ZWRQcm9wc0NhbGxiYWNrT25Nb3VudDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIF9wcmV2Q29tcG9uZW50OiBhbnk7XG4gICAgX3Byb3BzQW5pbWF0ZWQ6IEFuaW1hdGVkUHJvcHM7XG4gICAgX2V2ZW50RGV0YWNoZXJzOiBBcnJheTxGdW5jdGlvbj4gPSBbXTtcbiAgICBfc2V0Q29tcG9uZW50UmVmOiBGdW5jdGlvbjtcblxuICAgIHN0YXRpYyBfX3NraXBTZXROYXRpdmVQcm9wc19GT1JfVEVTVFNfT05MWSA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IocHJvcHM6IE9iamVjdCkge1xuICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgdGhpcy5fc2V0Q29tcG9uZW50UmVmID0gdGhpcy5fc2V0Q29tcG9uZW50UmVmLmJpbmQodGhpcyk7XG4gICAgfVxuXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICB0aGlzLl9wcm9wc0FuaW1hdGVkICYmIHRoaXMuX3Byb3BzQW5pbWF0ZWQuX19kZXRhY2goKTtcbiAgICAgIHRoaXMuX2RldGFjaE5hdGl2ZUV2ZW50cygpO1xuICAgIH1cblxuICAgIHNldE5hdGl2ZVByb3BzKHByb3BzKSB7XG4gICAgICB0aGlzLl9jb21wb25lbnQuc2V0TmF0aXZlUHJvcHMocHJvcHMpO1xuICAgIH1cblxuICAgIFVOU0FGRV9jb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgICB0aGlzLl9hdHRhY2hQcm9wcyh0aGlzLnByb3BzKTtcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLl9pbnZva2VBbmltYXRlZFByb3BzQ2FsbGJhY2tPbk1vdW50KSB7XG4gICAgICAgIHRoaXMuX2ludm9rZUFuaW1hdGVkUHJvcHNDYWxsYmFja09uTW91bnQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fYW5pbWF0ZWRQcm9wc0NhbGxiYWNrKCk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3Byb3BzQW5pbWF0ZWQuc2V0TmF0aXZlVmlldyh0aGlzLl9jb21wb25lbnQpO1xuICAgICAgdGhpcy5fYXR0YWNoTmF0aXZlRXZlbnRzKCk7XG4gICAgfVxuXG4gICAgX2F0dGFjaE5hdGl2ZUV2ZW50cygpIHtcbiAgICAgIC8vIE1ha2Ugc3VyZSB0byBnZXQgdGhlIHNjcm9sbGFibGUgbm9kZSBmb3IgY29tcG9uZW50cyB0aGF0IGltcGxlbWVudFxuICAgICAgLy8gYFNjcm9sbFJlc3BvbmRlci5NaXhpbmAuXG4gICAgICBjb25zdCBzY3JvbGxhYmxlTm9kZSA9IHRoaXMuX2NvbXBvbmVudC5nZXRTY3JvbGxhYmxlTm9kZVxuICAgICAgICA/IHRoaXMuX2NvbXBvbmVudC5nZXRTY3JvbGxhYmxlTm9kZSgpXG4gICAgICAgIDogdGhpcy5fY29tcG9uZW50O1xuXG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLnByb3BzKSB7XG4gICAgICAgIGNvbnN0IHByb3AgPSB0aGlzLnByb3BzW2tleV07XG4gICAgICAgIGlmIChwcm9wIGluc3RhbmNlb2YgQW5pbWF0ZWRFdmVudCAmJiBwcm9wLl9faXNOYXRpdmUpIHtcbiAgICAgICAgICBwcm9wLl9fYXR0YWNoKHNjcm9sbGFibGVOb2RlLCBrZXkpO1xuICAgICAgICAgIHRoaXMuX2V2ZW50RGV0YWNoZXJzLnB1c2goKCkgPT4gcHJvcC5fX2RldGFjaChzY3JvbGxhYmxlTm9kZSwga2V5KSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBfZGV0YWNoTmF0aXZlRXZlbnRzKCkge1xuICAgICAgdGhpcy5fZXZlbnREZXRhY2hlcnMuZm9yRWFjaChyZW1vdmUgPT4gcmVtb3ZlKCkpO1xuICAgICAgdGhpcy5fZXZlbnREZXRhY2hlcnMgPSBbXTtcbiAgICB9XG5cbiAgICAvLyBUaGUgc3lzdGVtIGlzIGJlc3QgZGVzaWduZWQgd2hlbiBzZXROYXRpdmVQcm9wcyBpcyBpbXBsZW1lbnRlZC4gSXQgaXNcbiAgICAvLyBhYmxlIHRvIGF2b2lkIHJlLXJlbmRlcmluZyBhbmQgZGlyZWN0bHkgc2V0IHRoZSBhdHRyaWJ1dGVzIHRoYXQgY2hhbmdlZC5cbiAgICAvLyBIb3dldmVyLCBzZXROYXRpdmVQcm9wcyBjYW4gb25seSBiZSBpbXBsZW1lbnRlZCBvbiBsZWFmIG5hdGl2ZVxuICAgIC8vIGNvbXBvbmVudHMuIElmIHlvdSB3YW50IHRvIGFuaW1hdGUgYSBjb21wb3NpdGUgY29tcG9uZW50LCB5b3UgbmVlZCB0b1xuICAgIC8vIHJlLXJlbmRlciBpdC4gSW4gdGhpcyBjYXNlLCB3ZSBoYXZlIGEgZmFsbGJhY2sgdGhhdCB1c2VzIGZvcmNlVXBkYXRlLlxuICAgIF9hbmltYXRlZFByb3BzQ2FsbGJhY2sgPSAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5fY29tcG9uZW50ID09IG51bGwpIHtcbiAgICAgICAgLy8gQW5pbWF0ZWRQcm9wcyBpcyBjcmVhdGVkIGluIHdpbGwtbW91bnQgYmVjYXVzZSBpdCdzIHVzZWQgaW4gcmVuZGVyLlxuICAgICAgICAvLyBCdXQgdGhpcyBjYWxsYmFjayBtYXkgYmUgaW52b2tlZCBiZWZvcmUgbW91bnQgaW4gYXN5bmMgbW9kZSxcbiAgICAgICAgLy8gSW4gd2hpY2ggY2FzZSB3ZSBzaG91bGQgZGVmZXIgdGhlIHNldE5hdGl2ZVByb3BzKCkgY2FsbC5cbiAgICAgICAgLy8gUmVhY3QgbWF5IHRocm93IGF3YXkgdW5jb21taXR0ZWQgd29yayBpbiBhc3luYyBtb2RlLFxuICAgICAgICAvLyBTbyBhIGRlZmVycmVkIGNhbGwgd29uJ3QgYWx3YXlzIGJlIGludm9rZWQuXG4gICAgICAgIHRoaXMuX2ludm9rZUFuaW1hdGVkUHJvcHNDYWxsYmFja09uTW91bnQgPSB0cnVlO1xuICAgICAgfSBlbHNlIGlmIChcbiAgICAgICAgQW5pbWF0ZWRDb21wb25lbnQuX19za2lwU2V0TmF0aXZlUHJvcHNfRk9SX1RFU1RTX09OTFkgfHxcbiAgICAgICAgdHlwZW9mIHRoaXMuX2NvbXBvbmVudC5zZXROYXRpdmVQcm9wcyAhPT0gJ2Z1bmN0aW9uJ1xuICAgICAgKSB7XG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcbiAgICAgIH0gZWxzZSBpZiAoIXRoaXMuX3Byb3BzQW5pbWF0ZWQuX19pc05hdGl2ZSkge1xuICAgICAgICB0aGlzLl9jb21wb25lbnQuc2V0TmF0aXZlUHJvcHMoXG4gICAgICAgICAgdGhpcy5fcHJvcHNBbmltYXRlZC5fX2dldEFuaW1hdGVkVmFsdWUoKSxcbiAgICAgICAgKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICAnQXR0ZW1wdGluZyB0byBydW4gSlMgZHJpdmVuIGFuaW1hdGlvbiBvbiBhbmltYXRlZCAnICtcbiAgICAgICAgICAgICdub2RlIHRoYXQgaGFzIGJlZW4gbW92ZWQgdG8gXCJuYXRpdmVcIiBlYXJsaWVyIGJ5IHN0YXJ0aW5nIGFuICcgK1xuICAgICAgICAgICAgJ2FuaW1hdGlvbiB3aXRoIGB1c2VOYXRpdmVEcml2ZXI6IHRydWVgJyxcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX2F0dGFjaFByb3BzKG5leHRQcm9wcykge1xuICAgICAgY29uc3Qgb2xkUHJvcHNBbmltYXRlZCA9IHRoaXMuX3Byb3BzQW5pbWF0ZWQ7XG5cbiAgICAgIHRoaXMuX3Byb3BzQW5pbWF0ZWQgPSBuZXcgQW5pbWF0ZWRQcm9wcyhcbiAgICAgICAgbmV4dFByb3BzLFxuICAgICAgICB0aGlzLl9hbmltYXRlZFByb3BzQ2FsbGJhY2ssXG4gICAgICApO1xuXG4gICAgICAvLyBXaGVuIHlvdSBjYWxsIGRldGFjaCwgaXQgcmVtb3ZlcyB0aGUgZWxlbWVudCBmcm9tIHRoZSBwYXJlbnQgbGlzdFxuICAgICAgLy8gb2YgY2hpbGRyZW4uIElmIGl0IGdvZXMgdG8gMCwgdGhlbiB0aGUgcGFyZW50IGFsc28gZGV0YWNoZXMgaXRzZWxmXG4gICAgICAvLyBhbmQgc28gb24uXG4gICAgICAvLyBBbiBvcHRpbWl6YXRpb24gaXMgdG8gYXR0YWNoIHRoZSBuZXcgZWxlbWVudHMgYW5kIFRIRU4gZGV0YWNoIHRoZSBvbGRcbiAgICAgIC8vIG9uZXMgaW5zdGVhZCBvZiBkZXRhY2hpbmcgYW5kIFRIRU4gYXR0YWNoaW5nLlxuICAgICAgLy8gVGhpcyB3YXkgdGhlIGludGVybWVkaWF0ZSBzdGF0ZSBpc24ndCB0byBnbyB0byAwIGFuZCB0cmlnZ2VyXG4gICAgICAvLyB0aGlzIGV4cGVuc2l2ZSByZWN1cnNpdmUgZGV0YWNoaW5nIHRvIHRoZW4gcmUtYXR0YWNoIGV2ZXJ5dGhpbmcgb25cbiAgICAgIC8vIHRoZSB2ZXJ5IG5leHQgb3BlcmF0aW9uLlxuICAgICAgb2xkUHJvcHNBbmltYXRlZCAmJiBvbGRQcm9wc0FuaW1hdGVkLl9fZGV0YWNoKCk7XG4gICAgfVxuXG4gICAgVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV3UHJvcHMpIHtcbiAgICAgIHRoaXMuX2F0dGFjaFByb3BzKG5ld1Byb3BzKTtcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICBpZiAodGhpcy5fY29tcG9uZW50ICE9PSB0aGlzLl9wcmV2Q29tcG9uZW50KSB7XG4gICAgICAgIHRoaXMuX3Byb3BzQW5pbWF0ZWQuc2V0TmF0aXZlVmlldyh0aGlzLl9jb21wb25lbnQpO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMuX2NvbXBvbmVudCAhPT0gdGhpcy5fcHJldkNvbXBvbmVudCB8fCBwcmV2UHJvcHMgIT09IHRoaXMucHJvcHMpIHtcbiAgICAgICAgdGhpcy5fZGV0YWNoTmF0aXZlRXZlbnRzKCk7XG4gICAgICAgIHRoaXMuX2F0dGFjaE5hdGl2ZUV2ZW50cygpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgIGNvbnN0IHByb3BzID0gdGhpcy5fcHJvcHNBbmltYXRlZC5fX2dldFZhbHVlKCk7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8Q29tcG9uZW50XG4gICAgICAgICAgey4uLnByb3BzfVxuICAgICAgICAgIHJlZj17dGhpcy5fc2V0Q29tcG9uZW50UmVmfVxuICAgICAgICAgIC8vIFRoZSBuYXRpdmUgZHJpdmVyIHVwZGF0ZXMgdmlld3MgZGlyZWN0bHkgdGhyb3VnaCB0aGUgVUkgdGhyZWFkIHNvIHdlXG4gICAgICAgICAgLy8gaGF2ZSB0byBtYWtlIHN1cmUgdGhlIHZpZXcgZG9lc24ndCBnZXQgb3B0aW1pemVkIGF3YXkgYmVjYXVzZSBpdCBjYW5ub3RcbiAgICAgICAgICAvLyBnbyB0aHJvdWdoIHRoZSBOYXRpdmVWaWV3SGllcmFyY2h5TWFuYWdlciBzaW5jZSBpdCBvcGVyYXRlcyBvbiB0aGUgc2hhZG93XG4gICAgICAgICAgLy8gdGhyZWFkLlxuICAgICAgICAgIGNvbGxhcHNhYmxlPXtcbiAgICAgICAgICAgIHRoaXMuX3Byb3BzQW5pbWF0ZWQuX19pc05hdGl2ZSA/IGZhbHNlIDogcHJvcHMuY29sbGFwc2FibGVcbiAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICApO1xuICAgIH1cblxuICAgIF9zZXRDb21wb25lbnRSZWYoYykge1xuICAgICAgdGhpcy5fcHJldkNvbXBvbmVudCA9IHRoaXMuX2NvbXBvbmVudDtcbiAgICAgIHRoaXMuX2NvbXBvbmVudCA9IGM7XG4gICAgfVxuXG4gICAgLy8gQSB0aGlyZCBwYXJ0eSBsaWJyYXJ5IGNhbiB1c2UgZ2V0Tm9kZSgpXG4gICAgLy8gdG8gZ2V0IHRoZSBub2RlIHJlZmVyZW5jZSBvZiB0aGUgZGVjb3JhdGVkIGNvbXBvbmVudFxuICAgIGdldE5vZGUoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fY29tcG9uZW50O1xuICAgIH1cbiAgfVxuXG4gIGNvbnN0IHByb3BUeXBlcyA9IENvbXBvbmVudC5wcm9wVHlwZXM7XG5cbiAgQW5pbWF0ZWRDb21wb25lbnQucHJvcFR5cGVzID0ge1xuICAgIHN0eWxlOiBmdW5jdGlvbihwcm9wcywgcHJvcE5hbWUsIGNvbXBvbmVudE5hbWUpIHtcbiAgICAgIGlmICghcHJvcFR5cGVzKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgZm9yIChjb25zdCBrZXkgaW4gVmlld1N0eWxlUHJvcFR5cGVzKSB7XG4gICAgICAgIGlmICghcHJvcFR5cGVzW2tleV0gJiYgcHJvcHNba2V5XSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgY29uc29sZS53YXJuKFxuICAgICAgICAgICAgJ1lvdSBhcmUgc2V0dGluZyB0aGUgc3R5bGUgYHsgJyArXG4gICAgICAgICAgICAgIGtleSArXG4gICAgICAgICAgICAgICc6IC4uLiB9YCBhcyBhIHByb3AuIFlvdSAnICtcbiAgICAgICAgICAgICAgJ3Nob3VsZCBuZXN0IGl0IGluIGEgc3R5bGUgb2JqZWN0LiAnICtcbiAgICAgICAgICAgICAgJ0UuZy4gYHsgc3R5bGU6IHsgJyArXG4gICAgICAgICAgICAgIGtleSArXG4gICAgICAgICAgICAgICc6IC4uLiB9IH1gJyxcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcbiAgfTtcblxuICByZXR1cm4gQW5pbWF0ZWRDb21wb25lbnQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY3JlYXRlQW5pbWF0ZWRDb21wb25lbnQ7XG4iXX0=